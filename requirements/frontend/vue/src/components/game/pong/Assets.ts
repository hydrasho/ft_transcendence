// Image importconsole
import imgBackground from '../../../assets/pong/back.png';
import imgBackgroundImproved from '../../../assets/pong/back_improved.png';
import imgScore from '../../../assets/pong/score.png';
import imgBall from '../../../assets/pong/ball.png';
import imgPlus from '../../../assets/pong/plus.png';
import imgFlash from '../../../assets/pong/flash.png';
import imgFlashR from '../../../assets/pong/flashR.png';
import imgWinR from '../../../assets/pong/winR.png';
import imgWinL from '../../../assets/pong/winL.png';
import imgYouWin from '../../../assets/pong/win.png';
import imgYouLose from '../../../assets/pong/lose.png';
import imgBonus from '../../../assets/pong/bonus.png';

export { imgBackground };
export { imgBackgroundImproved };
export { imgScore };
export { imgBall };
export { imgPlus };
export { imgFlash };
export { imgFlashR };
export { imgWinR };
export { imgWinL };
export { imgYouWin };
export { imgYouLose };
export { imgBonus};
