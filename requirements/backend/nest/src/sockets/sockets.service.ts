import { Injectable } from '@nestjs/common';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';

export enum ConnexionStatus {
  DISCONNECTED,
  CONNECTED,
  GAME,
}

export type UserConnexionInfo = {
  login: string;
  action: ConnexionStatus;
};

@Injectable()
@WebSocketGateway({ cors: { origin: 'http://' + process.env.IP_HOST + ':' + process.env.FRONT_PORT, credentials: true } })
export class SocketsService {
  private loginSocket = new Map() as Map<string, UserConnexionInfo>;
  @WebSocketServer() _server: Server;

  newConnexion(socketId: string, login: string, action: ConnexionStatus) {
    this.notifyStatusConnection(login, action);
    this.loginSocket.set(socketId, { login, action });
    // console.log(this.loginSocket);
  }

  newDisconnexion(socketId: string) {
    if (socketId && this.loginSocket.has(socketId))
      this.notifyStatusConnection(
        this.loginSocket.get(socketId).login,
        ConnexionStatus.DISCONNECTED,
      );
    this.loginSocket.delete(socketId);
    // console.log(this.loginSocket);
  }

  getUserbySocket(SocketID: string): UserConnexionInfo {
    const user = this.loginSocket.get(SocketID);
    return user;
  }

  getLoginBySocket(SocketID: string): string {
    const data: UserConnexionInfo = this.loginSocket[SocketID];

    if (data && data.login) {
      return data.login;
    } else {
      return null;
    }
  }

  getStatusByLogin(login: string): ConnexionStatus {
    const userSockets: string[] = this.getSocketByLogin(login);
    let status: ConnexionStatus = ConnexionStatus.DISCONNECTED;

    if (userSockets.length === 0) return status;

    userSockets.map((socketID: string) => {
      const newStatus = this.getUserbySocket(socketID).action;

      if (status !== ConnexionStatus.GAME) status = newStatus;
      else status = ConnexionStatus.GAME;
    });

    return status;
  }

  getSocketByLogin(login: string): string[] {
    const arraySockets: string[] = [];

    this.loginSocket.forEach((data: UserConnexionInfo, socketID: string) => {
      if (login === data.login) arraySockets.push(socketID);
    });

    return arraySockets;
  }

  notifyStatusConnection(login: string, status: ConnexionStatus) {
    const sockets = this.getSocketByLogin(login);
    const statusBefore = this.getStatusByLogin(login);

    // No sockets connected yet
    if (sockets.length === 0)
      this._server.emit('NOTIFY STATUS_CHANGED', 'change');
    else if (status === ConnexionStatus.GAME)
      this._server.emit('NOTIFY STATUS_CHANGED', 'change');
    else if (status !== statusBefore)
      this._server.emit('NOTIFY STATUS_CHANGED', 'change');
  }
}
