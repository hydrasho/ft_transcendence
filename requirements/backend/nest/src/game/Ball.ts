// import { Socket } from "socket.io";
import { Collision, Rect, Vector2i, Velocity } from './game.type';

function randomFloat(min: number, max: number): number {
  return Math.random() * (max - min) + min;
}

const center: Vector2i = { x: 220, y: 125 };

export class Ball {
  r: Rect;
  speed: number;
  velocity: Velocity;
  color: string;

  constructor() {
    this.r = { x: center.x, y: center.y, w: 12, h: 12 };
    this.speed = 1;
    this.velocity = { x: Math.PI / 4, y: Math.PI / 4 };
    this.color = 'rgba(40,40,40,0.5)';
    this.reset(1);
  }

  private collision_mur(x: number, y: number) {
    const border = Math.abs((220 - x) * 0.06) + 5;

    if (x < 25 || x > 430 - this.r.w) return false;

    if (y >= 250 - border) this.velocity.y = this.velocity.y * -1 - 0.06;
    else if (y <= border) this.velocity.y = this.velocity.y * -1 + 0.06;
    else return false;
    return true;
  }

  /**
   * create a friendship (the order has no meanings)
   * @param Ball_pos : Position x, y for the ball
   * @param rectP1 : rect from player 1
   * @param rectP2 : rect from player 2
   */
  private collision_players(ball: Rect, P1: Rect, P2: Rect): boolean {
    if (Collision.check(ball, P1)) {
      if (P1.x + P1.w - 1 > ball.x) {
        if (ball.y < P1.y) {
          this.velocity.y = -1;
          this.velocity.x = -1;
          this.r.y = P1.y - ball.h;
        } else if (ball.y + ball.h > P1.y + P1.h) {
          this.velocity.y = 1;
          this.velocity.x = -1;
          this.r.y = P1.y + P1.h;
        }
      } else {
        const middleBall = ball.y + this.r.w / 2;
        const middlePaddle = P1.y + P1.h / 2;
        this.velocity.y =
          (middleBall - middlePaddle) / ((P1.h + this.r.w / 2) / 0.7);
        this.velocity.x = 1;
      }
      this.color = 'orange';
      return true;
    } else if (Collision.check(ball, P2)) {
      if (P2.x + 1 <= ball.x + ball.w) {
        if (ball.y < P2.y) {
          this.velocity.y = -1;
          this.velocity.x = 1;
          this.r.y = P2.y - ball.h;
        } else if (ball.y + ball.h > P2.y + P2.h) {
          this.velocity.y = 1;
          this.velocity.x = 1;
          this.r.y = P2.y + P2.h;
        }
      } else {
        const middleBall = ball.y + this.r.w / 2;
        const middlePaddle = P2.y + P2.h / 2;

        this.velocity.y =
          (middleBall - middlePaddle) / ((P2.h + this.r.w / 2) / 0.7);
        this.velocity.x = -1;
      }
      this.color = 'blue';
      return true;
    }
    return false;
  }

  private reset(x: number) {
    this.speed = 1;
    this.r.x = center.x;
    this.r.y = center.y;
    this.color = 'rgba(40,40,40,0.5)';

    if (x === 1) {
      this.velocity.y = randomFloat(-Math.PI / 4, Math.PI / 4);
      this.velocity.x = Math.sqrt(1 - this.velocity.y * this.velocity.y);
    } else {
      this.velocity.y = randomFloat(-Math.PI / 4, Math.PI / 4);
      this.velocity.x = -Math.sqrt(1 - this.velocity.y * this.velocity.y);
    }
  }

  iterate(p1: Rect, p2: Rect): number {
    let movX = 0;
    let movY = 0;

    const Btmp = { ...this.r };
    for (let i = 0; i < this.speed; ++i) {
      if (this.velocity.x != 0) movX = this.velocity.x;
      if (this.velocity.y != 0) movY = this.velocity.y;

      Btmp.x += movX;
      Btmp.y += movY;
      if (this.collision_mur(Btmp.x, Btmp.y)) {
        return 0;
      } else if (Btmp.x <= 0.0 || Btmp.x >= 442.0) {
        const winner: number = Btmp.x <= 0.0 ? 2 : 1;
        this.reset(winner);
        return winner;
      } else if (this.collision_players(Btmp, p1, p2)) {
		if (this.speed < 13)
			this.speed++;
        return 0;
      }
    }
    this.r = Btmp;
    return 0;
  }
}
