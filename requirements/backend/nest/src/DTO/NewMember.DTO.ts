import {IsNotEmpty, IsNumber, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { MemberType } from '../database/enums/MemberType.enum';

export class NewMemberDTO {
  @IsString()
  @IsNotEmpty()
  channelName: string;

  @IsString()
  @MinLength(8)
  @MaxLength(20)
  @IsOptional()
  pass: string;

  @IsString()
  @IsNotEmpty()
  login: string;

  @IsNotEmpty()
  type: MemberType;

  @IsNumber()
  @IsOptional()
  mute: number;
}
