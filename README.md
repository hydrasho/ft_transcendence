# Suprapong 🚀🏓
Welcome to Suprapong, a single page application that allows you to play pong with your friends! (42 Common Core last project)

## Screenshots
Here are some screenshots of the project. More pictures and gifs are coming soon!

 PC Version | Mobile version
:----------:|:-------------:
![](./screenshots/home_pc_compressed.png)|![](./screenshots/home_phone_compressed.png)
![](./screenshots/loader_pc_compressed.png)|![](./screenshots/loader_phone_compressed.png)



## Project Information

### Frontend 🖥️
- **Technologies used**: VueJS, nginx
- **Improvements**:
   - **Improve views**: Enhance the lobby and chat interfaces for a better user experience.
   - **Add responsiveness**: Make the profile and home page more responsive to different screen sizes.

### Backend 🗄⚙️
- **Technologies used**: NestJS, PostgreSQL
- **Improvements**:
   - **Hosted backend**: The backend will be hosted to avoid launching website locally only.
   - **User account creation**: will be made available without the need for a 42 account
   
For any suggestions or bugs, don't hesitate to open an issue.
Happy gaming! 🎮