import store from '@/store';
import { Socket, io } from 'socket.io-client';

class SocketioService {
    public socket: Socket;

    constructor() {
        this.socket = {} as Socket;
    }

    setupSocketConnection() {
        this.disconnect();

        this.socket = io('ws://' + process.env.VUE_APP_IP_HOST + ':' + process.env.VUE_APP_BACK_PORT, {
            autoConnect: false,
            reconnectionDelayMax: 10000,
            withCredentials: true,
            auth: {
                login: store.state.login
            }
        });

        this.socket.on("disconnect", (reason: string) => {
            if (reason === "io server disconnect") {
                if (this.socket)
                    this.socket.connect();
            }
            // console.log('disconnected');
        });
    }

    setupSocketConnectionGame() {
        this.disconnect();

        this.socket = io('ws://' + process.env.VUE_APP_IP_HOST + ':' + process.env.VUE_APP_BACK_PORT + '/game', {
            reconnectionDelayMax: 10000,
            withCredentials: true,
            auth: {
                login: store.state.login
            }
        });

        this.socket.on('connect_error', () => {
            this.setupSocketConnection();
            this.reconnect();
        })

        this.socket.on("disconnect", (reason: string) => {
            if (reason === "io server disconnect") {
                // the disconnection was initiated by the server, you need to reconnect manually
                if (this.socket)
                    this.socket.connect();
            }
            // console.log('disconnected');
        });
    }

    getId(): string {
        if (this.socket)
            return this.socket.id;
        else
            return '';
    }

    disconnect() {
        if (this.socket.connected)
            this.socket.disconnect();
    }

    connect() {
        this.socket.connect();
    }

    reconnect() {
        this.disconnect();
        this.connect();
    }
}

export default new SocketioService();
