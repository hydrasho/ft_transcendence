/** Declaring class User **/
export class UserFormat {
  constructor(
    public username: string,
    public login: string,
    public using_2fa: boolean,
    public avatar_file: string,
  ) {}
}
