import { Subscription } from 'rxjs';
import { Ball } from "./Ball";
import { Bonus } from "./Bonus";
import { Rect } from "./game.type";

const center_x: number = 150;
const center_y: number = 125;

export interface Player {
	login: string; 
	score: number; 
	rect: Rect; 
	away: boolean; 
	discoAlready: boolean;
}

export class Game {
	p1: Player;
	p2: Player;
	timerSub: Subscription | undefined;
	ball: Ball;
	bonus : Bonus | undefined; 

	constructor(p1_login: string, p2_login: string, is_custom: boolean) {
		this.p1 = {
			login: p1_login,
			score: 0,
			rect: { x: 20, y: 40, w: 5, h: 40 },
			away: false,
			discoAlready: false
		},
		this.p2 = {
			login: p2_login, 
			score: 0,
			rect: { x: 430, y: 180, w: 5, h: 40 },
			away: false,
			discoAlready: false
		}
		this.timerSub = undefined;
		this.bonus = new Bonus();
		this.bonus.r.x = 0;
		this.bonus.r.y = 0;
		this.ball = new Ball();
		this.ball.r.x = center_x;
		this.ball.r.y = center_y;
		this.ball.velocity.y = 0.1;
		this.ball.velocity.x = +Math.sqrt(1 - this.ball.velocity.y * this.ball.velocity.y);
		this.ball.speed = 1;

		if (is_custom) {
			setInterval(()=>{
				this.bonus.randomize();
			}, 8000);
		}
	}
}
