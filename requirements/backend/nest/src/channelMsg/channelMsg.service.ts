import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { channelMsg } from '../database/entities/channelMsg.entity';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';

@Injectable()
@WebSocketGateway({ cors: { origin: 'http://' + process.env.VUE_APP_IP_HOST + ':' + process.env.VUE_APP_FRONT_PORT, credentials: true } })
export class channelMsgService {
  @WebSocketServer() server: Server;

  constructor(
    @InjectRepository(channelMsg)
    readonly channelMsgRepository: Repository<channelMsg>,
  ) {}

  // METHODS ************************************ //

  /**
   * add to the database the new message
   */
  async create(messageInfo: channelMsg) {
    const resp = await this.channelMsgRepository
      .save(messageInfo)
      .catch((error) => {
        throw error;
      });
    resp.channel.pass = '';
    if (resp instanceof channelMsg)
      this.newChannelMessageEvent(messageInfo.channel.name);
    return resp;
  }

  /**
   * get all messages for a specific channel
   */
  async getChannelMsg(channelName: string) {
    const messageChannel = await this.channelMsgRepository
      .find({
        relations: { channel: true, sender: true },
        where: [{ channel: { name: channelName } }],
      } as FindManyOptions<channelMsg>)
      .catch(() => {
        return { error: 'no discussion found' };
      });

    if (!Array.isArray(messageChannel)) return messageChannel;

    if (messageChannel.length < 1) return { error: 'no discussion found' };

    messageChannel.map((row) => {
      row.channel.pass = '';
      return row;
    });

    return messageChannel;
  }

  // SOCKETS ************************************ //

  /**
   * emit a new event on the socket
   * @param chanName
   */
  newChannelMessageEvent(chanName: string) {
    this.server.emit('#' + chanName);
  }
}
