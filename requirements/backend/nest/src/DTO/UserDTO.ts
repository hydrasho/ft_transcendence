import {
  IsBoolean,
  IsEmpty,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
  minLength
} from "class-validator";

export class UserDTO {
  @IsNumber()
  @IsEmpty()
  id: number;

  @IsString()
  @IsNotEmpty()
  login: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(12)
  username: string;

  @IsBoolean()
  @IsNotEmpty()
  using_2fa: boolean;

  @IsString()
  @IsOptional()
  avatar_file: string;
}
