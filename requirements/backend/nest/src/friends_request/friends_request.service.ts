import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { FindManyOptions, Repository } from 'typeorm';
import { FriendsRequest } from '../database/entities/friendsRequest.entity';
import {ConnexionStatus, SocketsService} from "../sockets/sockets.service";
import {SocketResponse} from "../game_lobby/game_lobby.service";
import {WebSocketGateway, WebSocketServer} from "@nestjs/websockets";
import {Server} from "socket.io";

@Injectable()
@WebSocketGateway({
  cors: { origin: 'http://' + process.env.IP_HOST + ':' + process.env.FRONT_PORT, credentials: true },
})
export class Friends_requestService {
  constructor(
    @InjectRepository(User) readonly UserRepository: Repository<User>,
    @InjectRepository(FriendsRequest)  readonly Friends_requestRepository: Repository<FriendsRequest>,
    private readonly socketsService: SocketsService,
    //readonly Friends_requestRepository: Repository<FriendsRequest>,
  ) {}

  @WebSocketServer() _server: Server;

  // METHODS ************************************ //

  /**
   * insert the friend request to the db
   * @param data :Friends_request request row
   */
  async createRequest(data: FriendsRequest) {
    const friends_req = this.Friends_requestRepository.save(data).catch(() => {
      throw new BadRequestException('Error relation already exist');
    });
    return friends_req;
  }

  /**
   * remove a request row from the DB
   * @param relationId :Number id of the request
   */
  async deleteRequest(relationId: number) {
    return await this.Friends_requestRepository.delete(relationId).catch(() => {
      return { error: 'the relation does not exist' };
    });
  }

  /**
   * return all requests (dangerous)
   */
  async getAllRequest() {
    const friends_req = await this.Friends_requestRepository.find({
      relations: ['sender', 'receiver'],
    }).catch(() => {
      return { error: 'friend requests not found' };
    });
    return friends_req;
  }

  /**
   * return an array of users that have received a friend request from the user
   * @param login :String user login
   */
  async getRequestSend(login: string) {
    const user = await this.UserRepository.findOne({
      where: { login: login },
    } as FindManyOptions<User>).catch(() => {
      return { error: 'user not found' };
    });

    if (!(user instanceof User)) return user;
    if (!user) {
      return { error: 'user not found' };
    }

    const requests = await this.Friends_requestRepository.find({
      relations: { sender: true, receiver: true },
      where: [{ sender: { login: login } }],
    } as FindManyOptions<FriendsRequest>).catch(() => {
      return { error: 'request not found' };
    });

    if (!Array.isArray(requests)) return requests;
    if (requests.length < 1) return { error: 'friends not found' };

    const user_req = requests.map((relation) => {
      return relation.receiver;
    });

    return user_req;
  }

  /**
   * return an array of users that have sent a friend request to the user
   * @param login :String user login
   */
  async getRequestReceive(login: string) {
    const user = await this.UserRepository.findOne({
      where: { login: login },
    }).catch(() => {
      return { error: 'user not foud' };
    });

    if (!(user instanceof User)) return user;
    if (!user) {
      return { error: 'user not found' };
    }

    const requests = await this.Friends_requestRepository.find({
      relations: { sender: true, receiver: true },
      where: [{ receiver: { login: login } }],
    } as FindManyOptions<FriendsRequest>).catch(() => {
      return { error: 'request not found' };
    });

    if (!Array.isArray(requests)) return requests;
    if (requests.length < 1) return { error: 'friends not found' };

    const user_req = requests.map((relation) => {
      return relation.sender;
    });

    return user_req;
  }

  /**
   * search if a relation already exists
   * @param senderLogin :String sender login
   * @param receiverLogin :String receiver login
   */
  async getRelation(senderLogin: string, receiverLogin: string) {
    const relation1 = await this.Friends_requestRepository.findOne({
      relations: { sender: true, receiver: true },
      where: [
        {
          sender: { login: senderLogin },
          receiver: { login: receiverLogin },
        },
      ],
    } as FindManyOptions<FriendsRequest>).catch(() => {
      return { error: 'relation not found' };
    });

    if (relation1 !== null) return relation1;
    return { error: 'relation not found' };
  }

  // SOCKETS ********************************* //
  sendNotifRequest(destLogin: string, srcDest: string): SocketResponse {
    const sockets = this.socketsService.getSocketByLogin(destLogin);

    sockets.map((socketID: string) => {
      const userInfo = this.socketsService.getUserbySocket(socketID);

      if (userInfo.action === ConnexionStatus.CONNECTED) {
        this._server.emit(socketID + ' FRIEND', {
          type: 'request',
          login: srcDest,
        });
      }
    });
    return { success: true, message: srcDest };
  }
}
