import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ApiService } from '../../api/api.service';
import { channelMemberService } from '../../channelMember/channelMember.service';
import { MemberType } from '../../database/enums/MemberType.enum';
import { channelMember } from '../../database/entities/channelMember.entity';

@Injectable()
export class OwnerRightGuard implements CanActivate {
    constructor(
        private apiService: ApiService,
        public channelMemberService: channelMemberService,
    ) {}
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const body = request.body;
        const cookie: string = request.cookies['jwt'];
        if (!cookie) return false;

        console.log('test');

        const login = await this.apiService.getCookieLogin(request);
        const channelName = body.channelName;
        console.log()
        if (!login || !channelName) return false;
        console.log('test');

        const memberData = await this.channelMemberService.getUserInfo(
            login,
            channelName,
        );
        console.log('test');

        if (!(memberData instanceof channelMember)) return false;
        console.log('test');

        if (!memberData) return false;
        if (memberData.blocked) return false;
        if (!memberData.register) return false;
        if (memberData.type !== MemberType.OWNER)
            return false;
        console.log('test');

        return true;
    }
}
