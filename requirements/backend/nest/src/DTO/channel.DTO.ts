import {IsEmpty, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength} from 'class-validator';
import { ChannelType } from '../database/enums/ChannelType.enum';

export class channelDTO {
  @IsEmpty()
  @IsOptional()
  id: number;

  @IsString()
  @MinLength(4)
  @MaxLength(10)
  @IsNotEmpty()
  name: string;

  @IsString()
  @MinLength(8)
  @MaxLength(20)
  @IsOptional()
  pass: string = null;

  @IsString()
  @IsNotEmpty()
  type: ChannelType;
}
