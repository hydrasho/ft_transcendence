import {
  Body,
  ConflictException,
  Controller,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { privMessageDTO } from '../DTO/privMessage.DTO';
import { privMsgService } from './privMsg.service';
import { UsersService } from '../users/users.service';
import { ApiService } from '../api/api.service';
import { Request } from 'express';
import { User } from '../database/entities/user.entity';
import { BlockUserService } from '../blockUsers/blockUser.service';
import { CookieAuthGuard } from '../guardians/cookies/cookies.guard';
import {blockUser} from "../database/entities/blockUser.entity";

@Controller('privMsg')
export class privMsgController {
  constructor(
    private readonly blockUserService: BlockUserService,
    private readonly privMsgService: privMsgService,
    private readonly usersService: UsersService,
    private readonly apiService: ApiService,
  ) {}

  /**
   * add a new private message to the database
   */
  @Post()
  @UseGuards(CookieAuthGuard)
  async add(@Body() messageInfo: privMessageDTO) {
    const messageData = this.privMsgService.privMsgRepository.create();
    const sender = await this.usersService.getSingleUser_bylogin(
      messageInfo.senderLogin,
    );
    const receiver = await this.usersService.getSingleUser_bylogin(
      messageInfo.receiverLogin,
    );

    if (
      !sender ||
      !receiver ||
      !(sender instanceof User && receiver instanceof User)
    )
      return { error: 'user not found' };

    if (sender.login === receiver.login)
      throw new ConflictException('users are identical');

    if (messageInfo.message.length < 1 || messageInfo.message.length > 1000)
      throw new ConflictException('message error');

    messageData.sender = sender;
    messageData.receiver = receiver;
    messageData.message = messageInfo.message;

    const getBlock = await this.blockUserService.getRelation(receiver.login, sender.login);
    if (getBlock instanceof blockUser)
      return {error: 'blocked'};

    return await this.privMsgService.create(messageData);
  }

  /**
   * get the list of users that hava a discussion with the user
   */
  @Get('discussionList')
  @UseGuards(CookieAuthGuard)
  async getDiscussionList(@Req() request: Request) {
    const userLogin = await this.apiService.getCookieLogin(request);

    if (!userLogin) throw new ConflictException('user not found');

    return await this.privMsgService.getDiscussionList(userLogin);
  }

  /**
   * get all messages between the user and the login
   */
  @Get('discussion/:login')
  @UseGuards(CookieAuthGuard)
  async getinfo2(@Req() request: Request, @Param('login') login2: string) {
    const primUser = await this.apiService.getCookieLogin(request);
    const secUser = await this.usersService.getSingleUser_bylogin(login2);

    if (!primUser || !secUser || !(secUser instanceof User))
      return { error: 'users not found' };

    const block = await this.blockUserService.getRelation(
      primUser,
      secUser.login,
    );

    const discussion = await this.privMsgService.getDiscussionWith(
      primUser,
      secUser.login,
    );

    if (!Array.isArray(discussion)) return discussion;
    return discussion.map((msg) => {
      if (msg.sender.login === login2 && block)
        msg.message = '--- User blocked ---';
      return msg;
    });
  }
}
