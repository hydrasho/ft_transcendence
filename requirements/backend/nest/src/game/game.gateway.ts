import {
  WebSocketGateway,
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketServer,
  SubscribeMessage,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { GameService } from './game.service';
import { ConnexionStatus, SocketsService } from 'src/sockets/sockets.service';
import { ApiService } from 'src/api/api.service';

export enum EmitMatchType {
  PLAYER1,
  PLAYER2,
  BALL,
  SCORE,
  PAUSE,
}
export interface EmitMatch {
  gameID: string;
  type: EmitMatchType;
  data: any;
}

@WebSocketGateway({ cors: {origin: 'http://' + process.env.IP_HOST + ':' + process.env.FRONT_PORT, credentials: true}, namespace: '/game' })
export class GameGateway implements OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    private readonly gameService: GameService,
    private readonly socketService: SocketsService,
    private readonly apiService: ApiService,
  ) {}

  @WebSocketServer() _server: Server;

  /**
   * Function called when socket client connecting
   * @param client socket used by client to connect
   */
  async handleConnection(client: Socket) {
    const login = await this.apiService.getCookieLoginSocket(client);

    // User already in game
    if (this.gameService.getSocketByLogin(login) !== undefined) {
      client.disconnect(true);
      return;
    }
    const matchID = this.gameService.getGameIdByLogin(login);

    const response: { success: boolean; message: string } =
      this.gameService.newConnexion(client.id, login, matchID);

        if (response.success) {
            // Add socket as GAME socket in socket lists
            this.socketService.newConnexion(client.id, login, ConnexionStatus.GAME)
        }
        else
            // Send error to client (handled by 'connect_error' in client)
            client._error({message: response.message});
    }

  async handleDisconnect(client: Socket) {
    const login = await this.apiService.getCookieLoginSocket(client);

    this.gameService.newDisconnexion(login);
    this.socketService.newDisconnexion(client.id);
  }

  @SubscribeMessage('match')
  onClassicMessage(client: Socket, payload: EmitMatch): void {
    client.broadcast.emit(payload.gameID, payload);
    this.gameService.updatePlayerPosition(
      payload.gameID,
      payload.type,
      payload.data,
    );
  }
}
