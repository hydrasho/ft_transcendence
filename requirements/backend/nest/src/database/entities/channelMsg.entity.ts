import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';
import { channel } from './channel.entity';

@Entity('channelMsg')
export class channelMsg {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => channel, (channel) => channel.id, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn()
  channel: channel;

  @ManyToOne(() => User, (user) => user.login, { nullable: false })
  @JoinColumn()
  sender: User;

  @Column({ nullable: false })
  message: string;
}
