import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { AppModule } from '../app.module';

@Module({
  imports: [AppModule], // New modules must be imported here
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersModule, UsersService, UsersController],
})
export class UsersModule {}
