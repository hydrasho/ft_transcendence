import {
  ConflictException,
  Controller,
  Delete,
  Get,
  Query,
  Post,
  UseGuards,
} from '@nestjs/common';
import { TfaService } from './tfa.service';
import { UsersService } from '../users/users.service';
import { ApiService } from 'src/api/api.service';
import { CookieAuthGuard } from '../guardians/cookies/cookies.guard';
import { User } from '../database/entities/user.entity';

/** Controller called when asking API on: `IP:back_port` **/
@Controller('2fa')
export class TfaController {
  constructor(
    private readonly tfaService: TfaService,
    private readonly userService: UsersService,
    private readonly apiService: ApiService,
  ) {}

  /**
   * create the 2fa if is not set or validate
   * @param userLogin
   * @param req
   */
  @Post()
  @UseGuards(CookieAuthGuard)
  async create(@Query('login') userLogin: string) {
    const user = await this.userService.getSingleUser_bylogin(userLogin);

    if (user === null || !(user instanceof User)) {
      return { error: 'user not found' };
    } else if (user.using_2fa) {
      throw new ConflictException('2fa already set');
    }

    return await this.tfaService.generate(user);
  }

  /**
   * check if the code send is correct and match to the 2fa saved
   * @param userLogin
   * @param code
   * @param req
   */
  @Get()
  async validate(
    @Query('login') userLogin: string,
    @Query('code') code: string,
  ) {
    const user = await this.userService.getSingleUser_bylogin(userLogin);

    if (user === null || !(user instanceof User)) {
      return { error: 'user not found' };
    }

    return this.tfaService.check(user, code);
  }

  /**
   * remove the 2fa for the user and in the database
   * @param userLogin
   * @param req
   */
  @Delete()
  @UseGuards(CookieAuthGuard)
  async delete(@Query('login') userLogin: string) {
    const user = await this.userService.getSingleUser_bylogin(userLogin);

    if (user === null) {
      return { error: 'user not found' };
    }

    return this.tfaService.delete(userLogin);
  }
}
