import {
  Body,
  ConflictException,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { UsersService } from '../users/users.service';
import { FriendsService } from './friends.service';
import { CookieAuthGuard } from '../guardians/cookies/cookies.guard';
import { ApiService } from '../api/api.service';
import { User } from '../database/entities/user.entity';
import { Friends } from '../database/entities/friends.entity';

@Controller('friends')
export class FriendController {
  constructor(
    private readonly friendService: FriendsService,
    public userService: UsersService,
    private readonly apiService: ApiService,
  ) {}

  /**
   * create a friendship (the order has no meanings)
   * @param friend_1Login :String first friend
   * @param friend_2Login :String second friend
   */
  @Post('add')
  @UseGuards(CookieAuthGuard)
  async addFriend(
    @Body('friend_1') friend_1Login: string,
    @Body('friend_2') friend_2Login: string,
  ) {
    const friend_1 =
      await this.userService.getSingleUser_bylogin(friend_1Login);
    const friend_2 =
      await this.userService.getSingleUser_bylogin(friend_2Login);
    if (
      friend_1 === null ||
      friend_2 === null ||
      !(friend_1 instanceof User && friend_2 instanceof User)
    ) {
      return { error: 'user not found' };
    } else if (friend_1Login === friend_2Login) {
      throw new ConflictException('Users are the same');
    }
    const relation = await this.friendService.getRelation(
      friend_1Login,
      friend_2Login,
    );

    if (relation !== null) {
      throw new ConflictException('Friendship already exists');
    }

    const friends = this.friendService.FriendsRepository.create();
    friends.friend_1 = friend_1;
    friends.friend_2 = friend_2;

    return this.friendService.createFriends(friends);
  }

  /**
   * delete a friendship
   * @param request
   * @param friendLogin
   */
  @Delete()
  @UseGuards(CookieAuthGuard)
  async deleteFriendShip(
    @Req() request: Request,
    @Body('relation') friendLogin: string,
  ) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    const relation = await this.friendService.getRelation(
      friendLogin,
      userLogin,
    );
    if (!(relation instanceof Friends)) return relation;

    return await this.friendService.deleteFriendShip(relation.id);
  }

  /**
   * return all friendships (dangerous)
   */
  @Get('get_all')
  @UseGuards(CookieAuthGuard)
  async getAllFriends() {
    return await this.friendService.getAllFriends();
  }

  /**
   * return all friends of the user (User[])
   */
  @Get('profile')
  @UseGuards(CookieAuthGuard)
  async getFriendsUser(@Req() request: Request) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    return await this.friendService.getFriendOf(userLogin);
  }

  /**
   * check if the user is friend with param
   */
  @Get('test/:login')
  @UseGuards(CookieAuthGuard)
  async testFriend(
    @Req() request: Request,
    @Param('login') friendLogin: string,
  ) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    const friend_2 = await this.userService.getSingleUser_bylogin(friendLogin);
    if (!(friend_2 instanceof User)) return friend_2;
    const response = await this.friendService.getFriendOf(userLogin);

    if (!Array.isArray(response)) return response;
    const stack = [];
    response.map((friend) => {
      if (friend.login === friend_2.login) stack.push(friend);
    });
    if (stack.length !== 1) return { error: 'friend not found' };
    return stack[0];
  }

  /**
   * return all friends of the user queried (User[])
   */
  @Get('of/:login')
  @UseGuards(CookieAuthGuard)
  async getFriendsOf(@Param('login') userLogin: string) {
    return await this.friendService.getFriendOf(userLogin);
  }

  /**
   * return all connected and available friends of a user (User[])
   * @param request
   */
  @Get('connected')
  @UseGuards(CookieAuthGuard)
  async getConnectedFriendsUser(@Req() request: Request) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    return await this.friendService.getConnectedFriendOf(userLogin);
  }
}
