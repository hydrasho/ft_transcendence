import {
  BeforeInsert,
  BeforeUpdate,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity('blockUser')
export class blockUser {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user1) => user1.id, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn()
  blocker: User;

  @ManyToOne(() => User, (user2) => user2.id, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn()
  blocked: User;

  @BeforeInsert()
  @BeforeUpdate()
  checkColumnEquqlity() {
    if (this.blocker === this.blocked)
      throw new Error('friend_1 and friend_2 must be different');
  }
}
