import { UserDTO } from './UserDTO';

export class FriendsDTO {
  id: number;
  friend_1: UserDTO;
  friend_2: UserDTO;
}
