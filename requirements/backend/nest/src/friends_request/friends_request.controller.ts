import {
  Body,
  ConflictException,
  Controller,
  Delete,
  Get,
  Req,
  NotFoundException,
  Post,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { Friends_requestService } from './friends_request.service';
import { FriendsService } from '../friends/friends.service';
import { CookieAuthGuard } from '../guardians/cookies/cookies.guard';
import { Request } from 'express';
import { ApiService } from '../api/api.service';
import { User } from '../database/entities/user.entity';
import { Friends } from '../database/entities/friends.entity';
import { FriendsRequest } from '../database/entities/friendsRequest.entity';

@Controller('friends_req')
export class Friends_requestController {
  constructor(
    private readonly friend_requestService: Friends_requestService,
    private readonly friendService: FriendsService,
    private readonly userService: UsersService,
    private readonly apiService: ApiService,
  ) {}

  /**
   * add the friend request to the DB. If the request already exists it throws an exception,
   * if a request send by the receiver exists, the request is deleted and a friendship is created
   * @param request
   * @param receiverLogin :String login od the user that the sender want to add
   */
  @Post('add')
  @UseGuards(CookieAuthGuard)
  async makeRequest(
    @Req() request: Request,
    @Body('receiverLogin') receiverLogin: string,
  ) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };

    const sender = await this.userService.getSingleUser_bylogin(userLogin);
    const receiver =
      await this.userService.getSingleUser_bylogin(receiverLogin);

    if (
      sender === null ||
      receiver === null ||
      !(sender instanceof User && receiver instanceof User)
    ) {
      return { error: 'user not found' };
    } else if (sender.login === receiver.login) {
      throw new ConflictException('Users are the same');
    }
    const friendShip = await this.friendService.getRelation(
      userLogin,
      receiverLogin,
    );
    if (friendShip instanceof Friends) return friendShip;
    if (friendShip) throw new ConflictException('they are friends');
    const request_send = await this.friend_requestService.getRelation(
      userLogin,
      receiverLogin,
    );
    if (request_send instanceof FriendsRequest)
      throw new ConflictException('Request already send');
    if (request_send === null) {
      throw new ConflictException('Request already send');
    }

    const request_receive = await this.friend_requestService.getRelation(
      receiverLogin,
      userLogin,
    );
    if (request_receive !== null && request_receive instanceof FriendsRequest) {
      await this.friend_requestService.deleteRequest(request_receive.id);

      const friendsMaker = this.friendService.FriendsRepository.create();
      friendsMaker.friend_1 = sender;
      friendsMaker.friend_2 = receiver;
      return await this.friendService.createFriends(friendsMaker);
    }

    const friends_req =
      this.friend_requestService.Friends_requestRepository.create();
    friends_req.sender = sender;
    friends_req.receiver = receiver;

    this.friend_requestService.sendNotifRequest(friends_req.receiver.login, friends_req.sender.login);
    return await this.friend_requestService.createRequest(friends_req);
  }

  /**
   * remove a request if it exists, with logins (it can be used for request sent or received)
   * @param request
   * @param receiverLogin :String receiver login
   */
  @Delete('undo')
  @UseGuards(CookieAuthGuard)
  async undoRequest(
    @Req() request: Request,
    @Body('receiverLogin') receiverLogin: string,
  ) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    const sender = await this.userService.getSingleUser_bylogin(userLogin);
    const receiver =
      await this.userService.getSingleUser_bylogin(receiverLogin);

    if (
      sender === null ||
      receiver === null ||
      !(sender instanceof User && receiver instanceof User)
    ) {
      return { error: 'user not found' };
    } else if (sender.login === receiver.login) {
      throw new ConflictException('Users are the same');
    }

    const request_send = await this.friend_requestService.getRelation(
      userLogin,
      receiverLogin,
    );
    if (!(request_send instanceof FriendsRequest)) return request_send;
    if (request_send === null) {
      throw new NotFoundException('the request doesnt exist');
    }

    const delValue = await this.friend_requestService.deleteRequest(
      request_send.id,
    );
    return delValue;
  }

  /**
   * Delete the request
   * @param requestId request id
   */
  @Delete()
  @UseGuards(CookieAuthGuard)
  async deleteRequest(@Body('request') requestId: number) {
    return await this.friend_requestService.deleteRequest(requestId);
  }

  /**
   * return all friend requests send to by the user
   */
  @Get('get_send/') // a modifier
  @UseGuards(CookieAuthGuard)
  async getRequestSend(@Req() request: Request) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    return await this.friend_requestService.getRequestSend(userLogin);
  }

  /**
   * return all users who send a friend request to the user
   */
  @Get('get_receive/') // a modifier
  @UseGuards(CookieAuthGuard)
  async getRequestReceive(@Req() request: Request) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    return await this.friend_requestService.getRequestReceive(userLogin);
  }
}
