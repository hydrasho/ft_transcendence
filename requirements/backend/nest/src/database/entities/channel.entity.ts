import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { ChannelType } from '../enums/ChannelType.enum';

@Entity('channel')
export class channel {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false, unique: true })
  name: string;

  @Column({ nullable: true, default: null })
  pass: string;

  @Column({
    nullable: false,
    type: 'enum',
    enum: ChannelType,
    default: ChannelType.PUBLIC,
  })
  type: ChannelType;
}
