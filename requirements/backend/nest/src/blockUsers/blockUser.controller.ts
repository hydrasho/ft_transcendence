import {
  Body,
  ConflictException,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { BlockUserService } from './blockUser.service';
import { CookieAuthGuard } from '../guardians/cookies/cookies.guard';
import { ApiService } from '../api/api.service';
import { Request } from 'express';
import { User } from '../database/entities/user.entity';
import { blockUser } from '../database/entities/blockUser.entity';

@Controller('block')
export class BlockUserController {
  constructor(
    private readonly blockUserService: BlockUserService,
    private readonly userService: UsersService,
    private readonly apiService: ApiService,
  ) {}

  /**
   * block the user (blocked) for the blocker
   * @param blockedLogin :String login of the user blocked
   * @param request
   */
  @Post()
  @UseGuards(CookieAuthGuard)
  async BockUser(
    @Body('blocked') blockedLogin: string,
    @Req() request: Request,
  ) {
    const blockerLogin = await this.apiService.getCookieLogin(request);
    if (!blockerLogin) return { error: 'user not found' };
    const blocker = await this.userService.getSingleUser_bylogin(blockerLogin);
    const blocked = await this.userService.getSingleUser_bylogin(blockedLogin);

    if (
      blocker === null ||
      blocked === null ||
      !(blocker instanceof User && blocked instanceof User)
    ) {
      return { error: 'user not found' };
    } else if (blocker.login === blocked.login) {
      throw new ConflictException('Users are the same');
    }

    const blockRelation = await this.blockUserService.getRelation(
      blockerLogin,
      blockedLogin,
    );

    if (blockRelation) {
      throw new ConflictException('the user is already blocked');
    }

    const createBlock =
        this.blockUserService.blockUserRepository.create();
    createBlock.blocker = blocker;
    createBlock.blocked = blocked;

    return this.blockUserService.createBlock(createBlock);
  }

  /**
   * remove a block relation if it exists, with logins (the order blocker blocked is important)
   * @param request
   * @param blockedLogin :String blocked login
   */
  @Delete('undo')
  @UseGuards(CookieAuthGuard)
  async undoRequest(
    @Req() request: Request,
    @Body('blockedLogin') blockedLogin: string,
  ) {
    const blockerLogin = await this.apiService.getCookieLogin(request);
    if (!blockerLogin) return { error: 'user not found' };
    const blocker = await this.userService.getSingleUser_bylogin(blockerLogin);
    const blocked = await this.userService.getSingleUser_bylogin(blockedLogin);

    if (
      blocker === null ||
      blocked === null ||
      !(blocker instanceof User && blocked instanceof User)
    ) {
      return { error: 'user not found' };
    } else if (blocker.login === blocked.login) {
      throw new ConflictException('Users are the same');
    }

    const blockRelation = await this.blockUserService.getRelation(
      blockerLogin,
      blockedLogin,
    );

    if (!(blockRelation instanceof blockUser)) return blockRelation;
    if (!blockRelation) {
      throw new ConflictException('the user is not blocked');
    }

    return await this.blockUserService.deleteRow(blockRelation.id);
  }

  /**
   * return all waiting request send by the user
   */
  @Get('userBlocked')
  @UseGuards(CookieAuthGuard)
  async userBlocked(@Req() request: Request) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    return await this.blockUserService.getUsersBlocked(userLogin);
  }

  /**
   * return all waiting request send to the user
   */
  @Get('blockedBy')
  @UseGuards(CookieAuthGuard)
  async blockedBy(@Req() request: Request) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    return await this.blockUserService.getUsersBlocker(userLogin);
  }

  @Get('isBlocked/:userLogin')
  @UseGuards(CookieAuthGuard)
  async isBlocked(
    @Req() request: Request,
    @Param('userLogin') userBlocked: string,
  ) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    return await this.blockUserService.getRelation(userLogin, userBlocked);
  }
}
