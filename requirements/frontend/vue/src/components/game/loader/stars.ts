


function int_range(min: number, max: number)
{
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

class Star {
	x: number;
	y: number;
	w: number;
	h: number;
	speedY: number;
	speedX: number;

	constructor() {
		this.speedX = int_range(0, 1);
		this.speedY = int_range(1, 4);

		this.x = int_range(0, window.innerWidth);
		this.y = int_range(0, window.innerHeight);
		let size = 0;
		if (int_range(0, 50) == 42)
			size = 50;
		else
			size = int_range(1, 10);
		this.w = size;
		this.h = size;
	}

	reload () {
		this.x = int_range(0, window.innerWidth);
		this.y = -100;
		let size = 0;
		if (int_range(0, 50) == 42)
			size = int_range(50, 100);
		else
			size = int_range(1, 10);
		this.w = size;
		this.h = size;
	}

	draw(ctx: CanvasRenderingContext2D) {
		this.x -= this.speedX;
		this.y += this.speedY;
		if (this.x <= -100)
			this.reload();
		if (this.y >= window.innerHeight)
			this.reload();
		ctx.fillStyle = "white";
		ctx.fillRect(this.x, this.y, this.w, this.h);
	}
}





export class Stars {

	array : Star[];

	constructor() {
		this.array = [];
		for (let i = 0; i != 50; i++) {
			this.array.push(new Star());
		}

	}

	draw(ctx: CanvasRenderingContext2D) {
		setShadow(ctx, "white", 42);
		for (let i = 0; i != this.array.length; i++) {
			this.array[i].draw(ctx);
		}
		endShadow(ctx);
	}
}

function setShadow(ctx: CanvasRenderingContext2D, color: string, force: number) {
	ctx.shadowColor = color;
	ctx.shadowBlur = force;
	ctx.beginPath();
	ctx.fill();
}

function endShadow(ctx: CanvasRenderingContext2D) {
	ctx.shadowColor = 'transparent';
	ctx.shadowBlur = 0;
}
