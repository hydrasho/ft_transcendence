import { WebSocketGateway, SubscribeMessage, OnGatewayDisconnect } from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { SocketsService, UserConnexionInfo } from 'src/sockets/sockets.service';
import { GameLobbyService, SocketResponse } from './game_lobby.service';
import { ApiService } from 'src/api/api.service';

@WebSocketGateway({ cors: { origin: 'http://' + process.env.IP_HOST + ':' + process.env.FRONT_PORT, credentials: true } })
export class GameLobbyGateway implements OnGatewayDisconnect {
  constructor(
    private readonly socketsService: SocketsService,
    private readonly lobbyService: GameLobbyService,
    private readonly apiService: ApiService
  ) {}

  @SubscribeMessage('game_lobby')
  handleClassicLobby(socket: Socket, payload: any): SocketResponse {
    const user: UserConnexionInfo = this.socketsService.getUserbySocket(
      socket.id,
    );

    // console.log(payload);
    if (user === undefined || user === null)
      return { success: false, message: 'Internal Error: Please try again.' };
    if (payload.action === 'request')
      return this.lobbyService.requestLobby(user.login, socket, payload.param);
    else if (payload.action === 'cancel')
      return this.lobbyService.deleteUserFromLobbies(user.login);
    else if (payload.action === 'invite')
      return this.lobbyService.invite(
        user.login,
        payload.param.mode,
        payload.param.guest,
      );
    else if (payload.action === 'join')
      return this.lobbyService.joinLobby(user.login, payload.param);
    else if (payload.action === 'ready')
      return this.lobbyService.validateLobby(payload.param, user.login);
  }

  async handleDisconnect(client: Socket) {
    const login = await this.apiService.getCookieLoginSocket(client);

    this.lobbyService.deleteUserFromLobbies(login);
  }
}
