
interface Vector2i {
	x: number;
	y: number;
}

export class Ball {
	x: number
	y: number
	w: number
	h: number
	color: string
	image: HTMLImageElement;
	snake: Vector2i[];

	constructor(img: string) {
		this.image = new Image();
		this.image.src = img;
		this.x = 220;
		this.y = 125;
		this.w = this.image.width;
		this.h = this.image.height;
		this.color = 'rgba(200, 200, 200,0.5)';
		this.snake = [];
	}


	draw(ctx: CanvasRenderingContext2D, ratio: number, shift: Vector2i) {

		if (this.color === "rgba(200,200,200,0.5)" && this.x === 220 && this.y === 125)
			this.snake = [];

		for (let i = 0; i != this.snake.length; i++) {
			const alpha = 1.0 / 15 * i;
			if (this.color == "orange")
				ctx.fillStyle = `rgba(200,100,0, ${alpha})`;
			else if (this.color == "blue")
				ctx.fillStyle = `rgba(0,0,255, ${alpha})`;
			else
				ctx.fillStyle = `rgba(200,200,200, ${alpha})`;
			ctx.fillRect(this.snake[i].x * ratio,
				this.snake[i].y * ratio,
				this.image.width * ratio, this.image.height * ratio);
		}


		setShadow(ctx, this.color, 35);
		ctx.drawImage(this.image, (this.x + shift.x) * ratio, (this.y + shift.y) * ratio, this.image.width * ratio, this.image.height * ratio);
		this.snake.push({ x: this.x + shift.x, y: this.y + shift.y});
		if (this.snake.length >= 15)
			this.snake.shift();
		endShadow(ctx);
	}
}
function setShadow(ctx: CanvasRenderingContext2D, color: string, force: number) {
	ctx.shadowColor = color;
	ctx.shadowBlur = force;
	ctx.beginPath();
	ctx.fill();
}

function endShadow(ctx: CanvasRenderingContext2D) {
	ctx.shadowColor = 'transparent';
	ctx.shadowBlur = 0;
}
