import { Module } from '@nestjs/common';
import { BlockUserService } from './blockUser.service';
import { UsersController } from '../users/users.controller';
import { UsersService } from '../users/users.service';
import { UsersModule } from '../users/users.module';
import { AppModule } from '../app.module';
import { BlockUserController } from './blockUser.controller';

@Module({
  imports: [AppModule, UsersModule], // New modules must be imported here
  controllers: [BlockUserController, UsersController],
  providers: [BlockUserService, UsersService],
})
export class BlockUserModule {}
