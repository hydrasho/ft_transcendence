import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Tfa } from '../database/entities/tfa.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { Repository } from 'typeorm';
import * as TOTP from 'totp.js';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class TfaService {
  constructor(
    private readonly userService: UsersService,
    @InjectRepository(Tfa) readonly TfaRepository: Repository<Tfa>,
  ) {}

  /**
   * general the TOTP token end return it
   * @param user
   */
  async generate(user: User) {
    const old_2fa = await this.find_row(user.login).catch((error: Error) => {
      console.log(error);
    });
    let tfa_row: Tfa;

    if (!old_2fa || !(old_2fa instanceof Tfa)) {
      tfa_row = this.TfaRepository.create();
    } else {
      tfa_row = old_2fa;
    }

    tfa_row.user = user;
    tfa_row.tfa_key = TOTP.randomKey();

    const tfa = await this.TfaRepository.save(tfa_row);

    tfa.tfa_key =
      'otpauth://totp/SupraPong:Transcendance?secret=' + tfa_row.tfa_key;
    return tfa;
  }

  /**
   * control the code with th token from the database
   * @param user
   * @param code
   */
  async check(user: User, code: string) {
    const row_2fa = await this.find_row(user.login);

    if (row_2fa === null || !(row_2fa instanceof Tfa)) {
      return { error: 'user not found' };
    }

    if (row_2fa instanceof Tfa) {
      const totp = new TOTP(row_2fa.tfa_key);
      if (!totp.verify(code)) throw new UnauthorizedException('Bad TOTP Code');

      if (!user.using_2fa) {
        const content = await this.userService.set2fa(user.login);
        if (content instanceof User) user = content;
        else return { isValid: false };
      }
      return { isValid: true };
    }
    return { isValid: false };
  }

  /**
   * remove the tfa from the database and from the user info
   * @param userLogin
   */
  async delete(userLogin: string) {
    const row = await this.find_row(userLogin);
    if (row instanceof Tfa) {
      await this.TfaRepository.delete(row).catch((error: Error) => {
        console.log(error);
      });
    } else return row;

    await this.userService.unset2fa(userLogin);
  }

  /**
   * fiend a specific row according to the user login
   * @param userLogin
   */
  async find_row(userLogin: string) {
    const row = await this.TfaRepository.findOne({
      where: { user: { login: userLogin } },
    }).catch((error: Error) => {
      console.log(error);
    });

    if (row === null) {
      return { error: 'error: 2fa not found' };
    }
    return row;
  }
}
