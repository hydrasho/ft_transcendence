import { PlayerProfile } from "./PlayerProfile";

export interface Rect {
    x: number;
    y: number;
    w: number;
    h: number;
}

export interface Vector2i {
	x: number;
	y: number;
}

export interface PlayersInfo {
	p1: {
		profile: typeof PlayerProfile | undefined,
		score: number,
	},
	p2: {
		profile: typeof PlayerProfile | undefined,
		score: number
	},
	id: number
}

export enum Activity {
	GAME,
	WIN,
	LOSE
}

export enum EmitMatchType {
	PLAYER1,
	PLAYER2,
	BALL,
	SCORE,
	PAUSE
}

export type EmitMatch = {
	gameID: string,
	type: EmitMatchType,
	data: unknown
}