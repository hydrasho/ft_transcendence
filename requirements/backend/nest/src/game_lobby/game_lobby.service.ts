import { Injectable } from '@nestjs/common';
import { randomStringGenerator } from '@nestjs/common/utils/random-string-generator.util';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Subscription, timer } from 'rxjs';
import { Socket, Server } from 'socket.io';
import { GameService } from 'src/game/game.service';
import { ConnexionStatus, SocketsService } from 'src/sockets/sockets.service';

export interface LobbyInfo {
  gameID: string;
  p1: {
    login: string;
    ready: boolean;
  };
  p2: {
    login: string;
    ready: boolean;
  };
  is_invite: boolean;
  sub: Subscription;
}

export interface SocketResponse {
  success: boolean;
  message: string;
}

@Injectable()
@WebSocketGateway({ cors: { origin: 'http://' + process.env.IP_HOST + ':' + process.env.FRONT_PORT, credentials: true } })
export class GameLobbyService {
  constructor(
    private readonly socketsService: SocketsService,
    private readonly gameService: GameService,
  ) { }

  private _lobbies: LobbyInfo[] = [];
  @WebSocketServer() _server: Server;

  requestLobby(
    login: string,
    socket: Socket,
    matchType: string,
  ): SocketResponse {
    const result = this.getClassicLobby(login, matchType);

    if (result.state === 'alreadyInLobby') {
      return { success: false, message: 'Already in a lobby' };
    } else if (result.state === 'alreadyInGame') {
      return { success: false, message: 'Already in a game' };
    }

    if (result.state === 'joinLobby') {
      this.validateLobby(result.gameID, null);
    }

    return { success: true, message: result.gameID };
  }

  deleteUserFromLobbies(login: string): SocketResponse {
    if (!this.checkUserInLobbies(login).answer)
      return { success: false, message: 'Not in any lobby.' };

    // Iterate through lobbies to delete player in them
    this._lobbies.map((item) => {
      if (item.p1 && item.p1.login === login && item.p1.ready) {
        item.p1 = item.p2;
        item.p2 = null;
      } else if (item.p2 && item.p2.ready)
        item.p2 = null;

      if ((!item.p1 && !item.p2) || (!item.p2 && item.is_invite))
        this.deleteLobby(item.gameID);
    });

    return { success: true, message: 'Correctly removed from lobbies.' };
  }

  deleteLobby(lobbyID: string) {
    this._lobbies = this._lobbies.filter((lobby) => lobby.gameID !== lobbyID);
  }

  joinLobby(login: string, gameID: string): SocketResponse {
    const match = this._lobbies.find(
      (lobbyInfo) => lobbyInfo.gameID === gameID,
    );

    if (match) {
      if (!this.checkUserInLobbies(login).answer || match.is_invite) {
        if (this.canJoinGameID(gameID, login)) {
          return { success: true, message: gameID };
        }
      }
    }
    return { success: false, message: 'Cannot join' };
  }

  invite(login: string, matchMode: string, guest: string): SocketResponse {
    if (this.checkUserInLobbies(login).answer)
      return { success: false, message: 'Already in Lobby' };

    const matchID = this.createLobby(
      matchMode === 'Custom' ? 'custom_' : 'classic_',
      login,
      true,
    );
    const match = this._lobbies.find(
      (lobbyInfo) => lobbyInfo.gameID === matchID,
    );

    match.p2 = {login: guest, ready: false};

    const sockets = this.socketsService.getSocketByLogin(guest);

    sockets.map((socketID: string) => {
      const userInfo = this.socketsService.getUserbySocket(socketID);

      if (userInfo.action === ConnexionStatus.CONNECTED) {
        this._server.emit(socketID + ' INVITE', { type: 'match', matchID: matchID });
      }

    })

    // Timeout invitation expired
    match.sub = timer(0, 500).subscribe((iteration: number) => {
      if (match.p1 && match.p1.ready && match.p2 && match.p2.ready)
        match.sub.unsubscribe();
      else if (iteration === 22) {
        this._lobbies = this._lobbies.filter((lobby) => lobby.gameID !== match.gameID);
        this._server.emit(matchID + ' CANCEL');
        match.sub.unsubscribe();
      }
    })
    return { success: true, message: matchID };
  }

  private getClassicLobby(
    login: string,
    matchType: string,
  ): { gameID: string; state: string } {
    let ret = { gameID: '', state: '' };
    const LobbyAvailableInfo = this.checkAvailableLobby(matchType);

    if (this.checkUserInLobbies(login).answer) {
      return { gameID: '', state: 'alreadyInLobby' };
    } else if (this.gameService.getSocketByLogin(login) !== undefined) {
      return { gameID: '', state: 'alreadyInGame' };
    } else if (LobbyAvailableInfo.answer === false) {
      const lobbyCreated = this.createLobby(matchType + '_', login, false);
      ret = { gameID: lobbyCreated, state: 'createdLobby' };
    } else {
      LobbyAvailableInfo.lobby.p2 = { login: login, ready: false };
      ret = { gameID: LobbyAvailableInfo.lobby.gameID, state: 'joinLobby' };
    }
    return ret;
  }

  private createLobby(prefix: string, login: string, is_invite: boolean) {
    const gameID = prefix + randomStringGenerator();
    this._lobbies.push({
      gameID: gameID,
      p1: { login: login, ready: false },
      p2: null,
      is_invite: is_invite,
      sub: null
    });
    return gameID;
  }

  validateLobby(gameID: string, login: string): SocketResponse {
    const MatchToCreate = this._lobbies.find(
      (lobby) => lobby.gameID === gameID,
    );

    if (MatchToCreate === undefined)
      return { success: false, message: 'Lobby does not exist' };
    else if (MatchToCreate.p1 && MatchToCreate.p1.login === login)
      MatchToCreate.p1.ready = true;
    else if (MatchToCreate.p2 && MatchToCreate.p2.login === login)
      MatchToCreate.p2.ready = true;

    if (
      MatchToCreate.p1 &&
      MatchToCreate.p1.ready &&
      MatchToCreate.p2 &&
      MatchToCreate.p2.ready
    ) {
      this._server.emit(gameID, 'match LAUNCH');
      this._lobbies = this._lobbies.filter((lobby) => lobby.gameID !== gameID);
      this.gameService.newGame(MatchToCreate);
    }
    return { success: true, message: 'OK' };
  }

  private checkAvailableLobby(gameType: string): {
    lobby: LobbyInfo;
    answer: boolean;
  } {
    const LobbyAvailable = this._lobbies.filter(
      (lobby) => lobby.gameID.startsWith(gameType) && !lobby.p2 && !lobby.is_invite,
    );
    let firstLobbyAvailable = null;

    if (LobbyAvailable.length > 0) firstLobbyAvailable = LobbyAvailable[0];

    return { lobby: firstLobbyAvailable, answer: firstLobbyAvailable !== null };
  }

  private checkUserInLobbies(login: string): {
    lobby: string;
    answer: boolean;
  } {
    if (this._lobbies.length === 0) return { lobby: '', answer: false };

    const lobbyIn = this._lobbies.find(
      (lobby) =>
        (lobby.p1 !== (undefined || null) && lobby.p1.login === login) ||
        (lobby.p2 !== (undefined || null) && lobby.p2.login === login),
    );
    let lobbyName = '';
    if (lobbyIn) lobbyName = lobbyIn.gameID;
    return { lobby: lobbyName, answer: lobbyIn !== undefined };
  }

  private canJoinGameID(gameID: string, login: string): boolean {
    const lobby: LobbyInfo = this._lobbies.find(
      (lobby) => lobby.gameID === gameID,
    );

    if (lobby !== undefined && lobby.p2 === null) {
      lobby.p2 = { login: login, ready: false };
      return true;
    } else if (lobby !== undefined && lobby.p2 !== null && lobby.is_invite) {
      return true;
    }
    return false;
  }
}
