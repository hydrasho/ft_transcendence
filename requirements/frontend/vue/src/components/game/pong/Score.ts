export class Score {
	image: HTMLImageElement;
	color: string;

	constructor(img: string, color: string) {
		this.image = new Image();
		this.image.src = img; 
		this.color = color;
	}

	draw(ctx: CanvasRenderingContext2D , pos: number, x: number, y: number, ratio: number) {
		const w = 26;
		const h = 40;

		// setShadow(ctx, this.color, 25);
		ctx.drawImage( this.image,
			w * pos + pos, 0, 
			w, h,
			x * ratio, y * ratio,
			w *ratio , h *ratio 
		);
		// endShadow(ctx);
	}
}