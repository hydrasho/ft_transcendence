import { Collision, Rect } from './game.type';

export class Bonus {
  r: Rect;
  time_p1: number;
  time_p2: number;

  constructor() {
    this.r = { x: 0, y: 0, w: 16, h: 16 };
    this.time_p1 = 0;
    this.time_p2 = 0;
  }

  randomize() {
    this.r.x = Math.random() * 250 + 100;
    this.r.y = Math.random() * 180 + 30;
  }

  is_collision(r: Rect): boolean {
    return Collision.check(this.r, r);
  }
}
