import { Module } from '@nestjs/common';
import { Friends_requestService } from './friends_request.service';
import { UsersController } from '../users/users.controller';
import { UsersService } from '../users/users.service';
import { UsersModule } from '../users/users.module';
import { AppModule } from '../app.module';
import { Friends_requestController } from './friends_request.controller';

@Module({
  imports: [AppModule, UsersModule], // New modules must be imported here
  controllers: [Friends_requestController, UsersController],
  providers: [Friends_requestService, UsersService],
})
export class Friends_requestModule {}
