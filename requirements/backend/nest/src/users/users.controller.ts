import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Patch,
  Req,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { Request } from 'express';
import { ApiService } from 'src/api/api.service';
import { CookieAuthGuard } from '../guardians/cookies/cookies.guard';
import {UserDTO} from "../DTO/UserDTO";

/** Controller called when asking API on: `IP:back_port/users` **/
@Controller('users')
export class UsersController {
  constructor(
    public userService: UsersService,
    private readonly apiService: ApiService,
  ) {}

  /**
   * create a new user into the DB
   * @param data
   */
  @Post('add')
  async newUser(@Body() data: UserDTO,
  ) {
    if (data.avatar_file.length === 0) {
      data.avatar_file =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAABwCAYAAADG4PRLAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAB9uSURBVHgB7V0JfFTV1f+/ZebNmkwS1kBI2FHQACKLFBCRpVW0Cu7fVyyCS92r1tavn2utrdrWn8WlKkVbrYpWWUQFEVwQQQggsu87JCHr7DNv+c6974U1iRPkDUy//PN7DMm8ue+9e+4995z/OfeMoKvGXjQjYyGiGRmNZgFmOJoFmOFoFmCGo1mAGY5mAWY4mgWY4WgWYIajWYAZjmYBZjiaBZjhaBZghqNZgBmOZgFmOJoFmOGQcQqgL9uLxPyNMA6GIZ3dFo5RZ0Bo60UmQd9ajfjX22Cs2g20DsA9theEHnlIN4R0BnSNighqRr+ExKod/HcNOgT6MejH1aM93PdeANdPzwICCk5HGNVxJGatRfC+mUhUVNFfdJhKTOBH1sie8L1yDdDOh3QhbQI0tlej+sJnkdxZQ4+qQirKg6NbWyS3HURsSxmdodIhQenaGr7fjoFyXR+cNjCA+PNLEXxqARJ7DoINPSY8pXMupPxcqOXVSG4o42J0FrZDTsldEAJupANpE2DoZ28h+q/FENrlIusxEtC1fenqNHp1HeqXOxB9uwShN76BHkvyGekZWYys58ZD6pSLUwltXRmCE99EdPk2sNkmZTng+UkveG8eDHFgoTn5aCKqbyxD5d0zYAQj8Nx4IXzPX4p0IC0CNLZVobLbw1xhBqZdBee159Z7nra1HOE/fIrafyyhrqLO8nngf/IyuCf2p9VaQLoR/dMihB6cBSPO7lyD++re8PzmQkjdW5qD79jzfzcX1Y/NBdMkrTf+L4TO9g++tAgwctt7CL+4EPJZBchddle9D38kEit2Ivjfb5BqrQAb3t6L+yJryjgI7bOQDjB1X/Pf/0RiyRawKab0LoLvj2MgD+tizriGEE+ivOhRqJURZN//Y7gfHwO7kRY3IkbqhT25Z1zx9wqPwdm3EHmf3YHsSefx/op8sBoHhz0D/as9sBvJT7egcsCfSXjbaOiI8E0+D9nzJkE+/3uEx6A44bm2DxxslZ+9BumA7QI0VpchEQwBbqlphklLH3zPjUOrN66H1NIDbWcVSoc9hfhjC2AXYo9+iuqxL0OrDEMg16DVrEnwTrkcQrYrxRbImh7PnlFAch0NtogGu2G7AJPrD/DLCO1yIBQEUvzU4aEuj++NlnNvhlLcnlYWoPKRmYhMeod6W8dJg2ogNHE6ah/9EEYiCqVfB7QsuRPS6O4pfPjoaSn1aAOxpZ8Zrnw22w3bHXl15S7+KrYgR11gIjBS+JRx1P/FnvnInTMZoVveQ3D2KoReXQwjpML7wmVAjjU72GBffxAJMukRTQLhOC2fBm+Ka21Sb0a2AkduFoSercBHA2u9Iobw3TQoyEIGKT/fXSPg/Z8RQFZDs06w7q/u9ZjnIR9WdDvALmCsoFk4NpVBcOKwXYD6qn00/3RSg36kJrz6QJ9r6YXvnWsh3JuN0JTFiLxbApXUqtSzBZLzN0HdU0UyJP+SrqZxabJOVNncp6vr/FWjdmQ2IGQF0jnt4ejSBup3+xBevZu/n33f+fCQi9P4Wmcc83oM2GhhM3BXNbSk/SrUdgFq4Rh1DnVcj5ZN+NSxo5xeE0kk3lkLvSxoCYSMvmXbICzbTmcwM5/cDi40U0hMXCKcEGQDmppAkg0i+mFCVNUohKWbEVu6jbcus09LMrQ9RI/9qwTKT3sB3hNng+RWPvMO1+6H3bBdgEZFmI15SB5Pip84YvgnVajf7OFWbOStb2GEVS48iW5bdJKn2CUPzv4FkM9sCbEgB2K7AOSCLG4AwUXCFKy2DBITDSRjbwjJ/TXUTgz6hgPQllPbJbuh00xWVRWhN5dBf7MEjt/mwHNBZ7hvPg9Sv4Lvtz4PDTQTIrk7bFChOgG7Ya8A6Zl08uXYw0h5TaOW1CW7Ebx3NpLL9tHnmSqiES2SWX9xTzivKYZzQCEZRiQsZyqPQB2c5YFAh/OMVuafxp7FX3y0ThpbaO1ctBXR12igfLUDiX1VSLy+DLWvfwP3oC7I/utPIZ7VFo0+6JFCJBqNKW5VjcFu2CtA6ndNT1LX02X8qagkIrY3lqLml3MQn7+Zi01inGPffPjuuRDypd1/kGqrFyLR6d1aQmHHxIEIBOOIf7wBwQc+QnzLPiS+3oLSfs/AO6Yr/M8SmVCY00BDxqFn0CVTVcs02+2GvQJkViAbiez/7voudXiNM6qjiBN1VfvkZ5wLFVwyPIML4H/sJ5AGFSJtoIGmXFEMZfzZ0JbuQs1DJMh5mxEmocb7/AmeWwfDe8cQU03X+zxcUfD1VzOkw5PTJtgrQKKUNG4XkjXoq0+FmqNWK9mLmqv+BW13JTdOnGe1hvf5cZB+VIRTBlo/JSKrcz++Ger736H64Y+Q/G4/Qk8uIuL9W+R9OBlCl/rjf3J+Fh+EhmE/f2uvI68b3IUQyDrUpfofRv3nKlQNfx767nJaJ4m8fuBCZK26+9QK70jQbcuXn4UWK+5B9q9HQvSKFBKrQvk5T0N9YUk9H6BB6ZK43uGGjM2wVYCMxTc9ZgOOvDor9LAgYw/PR+Wk6XQerRdntkVg/mQoj48yddDpBlmC54mfIHfWDZyrNYgJKr9rOmKk8mGw5zzC5aF1mrk1Avu7dqK+b2qwtadUjRgRrkwkCLl1KtTgDxx7/DMEn5jP4/HKqC4IrLgLYu92OClIkPkTIiamNmYeITrUkzMbpAu6IPfDn8N9bgcwYQUfnIvwg/NwlBvhd3NPVI+qJitkI2xdA0XmRtCDyEyVyNKhv8d/Px+hR7/gC73nml5wP385UV1NvxVjVyU08hMTJfuhrdoNrSJCAVVy2cP0GqPO01Qz2UHQYRC9JbT2Q3CTie90kf/YGs7BnSAVt2sCR2uCtZO15A7gkmmIztlA6+Kn5LxnQbl9MH+fLReMDRJiUXpYGkx+2AZbBSgkwYktRmghm6lQGpXvfIvw459zdsY7oS+UV8YdJdzGYOyvReLDtYjOWEcW4g5o5VHOuAiWmjY4p2KyMrB8R53PDEYAhKHvqeHnMZYkvGAt/Z9FNhxwtJRJmF3guqwnXBeRf5iXAulA7kfWexMgjPsHQh+spcDvXMjkwEuX9aIVgN0BDZiYZArQRtgqQIMxIDy2bvClkPGD1TfNhKBRkHRAAZSpV9DfG7fUjPIwtGW7EfvTQkQXbLMUlc4PwUnkWG4AQhsfHK1zifTOg5jn41YgWrj5wNB1OlenTjwYQXJ7FfRqYmO2VsKg3xPby6GVhaCWx6HNWIswHSKmUwD5DHhuOA/y4ELOwTYIur7vH1dDG/USEsv3ovrOmcijGa0LJgPLohxI2mvI2OtG7K/maxxToiBiN3zFG6TeonB3bAHPzAmNCs/YWI74tBJEXv4aemXUsudoZAdotozqCufQTlBGnkFR+myTjRG+32R3HvuHaAL6tkokiDSILNhM7M82JCui5oz6YD0Uvwvue4bBPXkghPz6swGEHDeyZ/wclb3+CK00iNB9M+GZPMR6V7XbDbRZhQbZAi7wh4j/9StEVu2EQxLhfoXWvNb1LwwsVzT+0lJEn/gMWihi3iKFdjxje8AxqhuUq/t876xNGW4nzdo2cLHjTur0cAJxmoWJOSTA2WsRC9IsfXgeIs8sgmfiAHgeqj/MxCg9/zPjEbz+bUQ+2ATXkK6w3wO0rm1nToz23jqUX/kinz2Kz0sxvBite/3gmXZdvcMy8doKMnC+QGJTmTlzs5xw//JHUIj7FDrlpTTLThaM7ZWITl2C6OsrKFRVw7WIXJiNrJfGQ6aBdBzI5w3/9FVEZ6/jRDrLrmMP2WLLAxCKmmYkNQX2CvCd71BxzdS63yD7Aggsvx1S91ZHn1gaQvTO2cRwfMeDQoZLgPtXQ+H61RAzqnAKYZSFEZ3yBUIvUhC5MsRXdO/Y3vC8ftVxs9HYVI7K7k9xs8rgZ2rIXX4PuUetYRdsFWD0tpnE8C+G2LUFNDLrvaT+3I8cnall7KN1Y9TfkFzLMtDIuKEItmfKpWak4TSCTmty6DezEZ61huaiwslv//xJx7kgxroy6GQgaQkVsSfmQttai+xFd9i2dcA2ASaf+xrqlxvgmXoN+UHWSD1GA+pbK1B7/vNcRUkeBf6nL4I8eUBaVWWTQFZ19G9fo/bO97klLRW2QN4XvwA6NDzYwg/NBdaVw/PGdSdv7T4C9jAxZLXEXvgK7ueuMNWMuXXgaETiCF/yGo+CS60C8L8/AfKNA09f4THQvbEgb94smnk5CgWCK1F76asAuToNwfvIaOgUmdfmbIQdsEeAVVGIe2nhb8SHij06H7F1pUQ7+ZH95lWQR3RBpkAe3R05824iI0tGbNVeRJ/5vPHzW2VDPxCEHbBHgOS86o04sFx1/nEhd/Dd/zMc4rDOyDTIfdrD++AYzuuE//wlDNIkDYHl4tjlV9ikQg0eBWwI8ZeXct/QMbCA/K/ByFS4bx0IuaOfXAYdiX+WfM/Z9rjcNkYjGp6B0cU7uaEtDyRG39HEW2BRhZqTnGvCKL/oCSQgOcgevbIPD1gn1pU1cqIIWbQnrGTPsBDYet+wYGQasTxS729iotP8TQhOepdnezkv6AjvNPLFUk57b6DNz7cieMsMousicAzoAP9Ll1G0IXUXRu7WAjzrJxhp8ByJb2U9lEt8UmHTDGxc4WvJkBk7OKMFUoW+9SCCE6aTy0H0ViiB0KwNCE5825w9JwqKbgQnvkvMzz6oB6sRm7MOYTZAmtCm2MLHXXasKm/wc8yxF2yyrtOvQukhjZAV4slN3bkN3/8hkkQWGzw4ZUYjYjM38Fl5ogg/NA+xnYxAkHm2KQv8RD/awGdlqjA8Th6y0nW1wciDwedeJgnQqKOw6wFFqPVatt44IPqcqbWXpDVmya5DSQsqj3CI3AJUF27HCYE55Yu28A7Q+Q8OtR6fuyHlZtjMktidEaOE0tqGz7Mps8IeAfLBVr/GN2itQbXKT5JTJHn1NaVIHKi1hoTBRWdYfKP63Qmmr7OMuU2VXL2ZMcu64DNZyUt3p9yM6GNrMMt9oXmo1i8llpisC/bEBe0RoEsmQrr+po0oPUyCMfV6IzuAjoYejltdLPN4u87HvGbOnhO1SINxmIUVVN6m2fWS+RqKpNyMkOfmlTa4zmkw+q7blp9mjwAVBwylAZurImJOUJeTx+NSgdiZ5V/q1lE3D81/xawTtELZplF+J9KhNAxzDxP9eFNfm0XicAW3lUC46WC95zTuFf8w2NMuqRMhWX82llEe4q9Sljtl3lNsm03mekuuOEWrq1X+G3V57zY4IXgp2l5cwEuemPk0Gm+bsSYs3SNVsJil4HebxSoqG5q59qVV2DQDSdW563cx2Z4+rnBcTfOK3ON68xmiH0oV1vh+Uff4s3GicF59Nhfa4WI95sqqjC9uQiNkjClOs50G1Dmfn5I9XW1PqyxFpWMe9FnrjntL21HFx6Mjv2llqdx3DYbCkpb4Tj9mxRrw/nIYhB+QS8pqvTjPLeBshmTVjPLcPhRSv/apNyLSYJJd/NN6+Pg1kHGkjPsVR9jD99qWE+P63aWovervcO+tgtwmC+KPOkJoRbxheYR3lNTaWrtq4tD3VkNk274aUalCngc5cycj9FsKkpbWwH3xGVAmDsIPgl9Bzr+vR+RXHyFRHoR7aFcot6fQJluAKdqCtlk8xid0DEDcXgUjZtFxwRji764BtpUh8vJy+KdeC6G9PcmhtglQHFwAz+OXIHjDm3yLmH/+TZBG+GEkktxc163NLtqS7agY/xqZ4wrkc9rDObQIzq5tIQ/tBByzp1CggeB/5QqcVOQH4Hn9GjSWCWqQ1kgu2wuVpSEu24XEwh0899P/9rWQh1MYrBXbqUTOjeXIq59vR3jidK7evFOuhDTGvlCZvZnZgwuhc1vv8G5ZqSpubmvuZeaJSKN7oOXW30JnuZ/Ld0BbsR/Bad+QnxZEYOm9EIpOoAIgXUB9nJibOeSQt/DA9cQlEHrl44RAdFtF76chBTw8k1seUAjPDYMgntUaQrtsfoqc6zEzsWui/HddkayNLU7IY+yNc9q8xdqwfCQNdYa/Gk6aGdR5h10IgUx6idYI7/BOfE1hLD8qwg0n1cYT3HhoUOWSKqt8eB5c/TrxdAb93tnwfnwTGoTGHHHNbPNYkJpssfdBwMPu17resZctyDadnO1V/FdRlqwNBXXVDO2DvTOQP4hhPa/11BU1XKVCNK3Q6GPzEXz1G3I7qAPJ8zCEBES3C/5euVCmTQByj1FusZh5JKiz/Q0ImBgRh49U4+9/jORX25Gcu67hm2QEdCRkEdGe44WoGUhc/hqMmjDi5XG6RdIn2W5+/857BsF1TV9IXpMAEKKWu6BpXPNIMtmmSgYLUKDO17ngyGur225M/CfzvLQVe4Hrz4H7gRFwXd/f3FHkYE40dSCpK9Oyr2eGxS1Dge18Yp2+uxoGsSpCz8P+YPThT+A4N5/XNmOF6UJ/WQh9+mqIVx52OVgBPmZ88PZ0wxxfGruzYwRIRopz+s94sQZHQj2888lF7sOZ5jKQXFNu7s6welMt2W3yRW3zrPXRPtg7PMjJdfVlTrGAxCdm1EA+v7NZ/+y1b6CzQjjMiivIgtA5B0KHAJ9xrDCs/s2O+sMzRp1TzBY6Irnvn4Po6KnQl+8hoVQiwvJLp62A58mLzbbbBZD1h0twcPLbiNHaqm8iQ+T1ElR0+z1iLyw2900cart+LlPfVQGBBpXYozXEs/MhntcRYt/2XIgqDQyWSc4iEtLQQn6+SbATKcBKq9gRBDwCtpcZcVx7DpIrtkP9YD35RLVw3zmMTGvqyOoIKi98BTkfT4TUvwP0daVIfrkDybdWQl1RBqVfAO73bzyeL2WBYsPqdFJjjsvOhvbdAYQHvkBPI8JB7kVgxnUQ+xw2WpQbByHX4UTwb4sRuuXfEEjFKr3z4WC5OBL1sGqxRvXNeFofa295GypF3OV+BXDdMBDKeTQI2weg/qMEoTve57PA0S0Pzsn9yWKtRGzRVh72cvysP+yG7eUmjcooavs8Sb5eLbz3XgDlqYto5u1FzWWvQd1Vy+Uhdcmh/1fwemjOIZ2gXFdsWo31dWjdGqgo5N1bbgZLs9hYBhTlfH8VC0530UzL8Zjts0kXtMJAHh8fBMchkoD6yUYkF25DbOFmqGtL4SgkomJnLc8klyiq4vt4Aq92EbryVUTf/ZYEnIu8Lf9re51T2/cys525nluHUz8lEXn6c+gUhBX7tkP2ZzdBGVLIt38lN1Wwwr2Qh3Q090E0JDwGF81If9Zh4TGwTmdrYColSJhRxALJwhEWpZcE5/bULzwGskDli3tBGt4Zju6tudA0ixJ0jukG34IbIHRvhTjNyChz4EmhBn5zUVqK1Kan5DKrBjj6b9zBRY4XgU/pgfu0g1EVReKdlQg++imvoqRzslojVdUBrkt7Qjmf1ppzi05JtV4OCj5rK/cg/sFaxIgW1DccNPe+sxyY/Cx4HxkB6Yb+fDCof1+Gqhum0zMk4b2sD7zvTEA6kLaa2RrxgaERL/BMbNHjQWDxLRCKzepHxr5axGasQejRT6CWBmlhlqyddQacBblwnNkSzou7Q7mgCxk7rewTKFmZrORJfNE2JD5eC3VrNd1vrblB1Sqjx4q1+x4aCemSXof2/WsLNqNm3KvQquOQOuYi8NWdRBum52sU0vq1AzqtHTUjp0AvCxGzEYD3hUspItD7iBMoqLN2P+JvrUbszZVIbq+0KhCyDmRchwyZAsVC2wDkYnITWmaRpZcLMS8AZ6dcGMSXig5630WqtM7/MnMmuIXJg79svaRBEt9+kO/a1TaUIUlcbHLNAYDt1lUThwK0DA4mONII7pFdoIwr5ur/SPUef24xIvfMQjIeh6NjG2R/ciOEohykC2kVIEN49IuIfLqFj2fDLcH/4Cgo9w8/nt1gxSw2lyH+3mokv9iOGFl3OnGReswUpmFuYraSK2QeoZetiL0ZHDIT+cw4n2gFi9g7qlXrMHn0xSww31XM9sDdpwDi+V3g/q8+psFz3IMkEP/1XASnLOLXYEFbz4QB8E69HOlEegVI/VTd8Xfmhsl8L99axrrbc1EvuKcQX9nYyGXZbGy//NJdSG4uRZxIZW1fGEY0BmEfsf97K8HzbKxNzcKhTEzdErBh7dY3v26E5ePorUlYATekNl44eubzdHmJZrbQwtNoZERbX4rITTMR/9Isii4yK3pLOZx9OiN72S+QTqRXgBsrcaDno1wt5a6+F5G/fonYy8v4WyIZN+7fjYD7xsENW4MNgTngjE0hFahvoZmaVE2ViboiC0weNOe9FD3vSoMkh61dQtMz/cpDiLJis898BqPWTMjwPH0RRJq1QYo+CLKM3OjvbdsHUR/S+t1JyRW7+AW5mqP4n+8lCrWQNRq7/2OoVSEEb/03Ocer4H5qLLkURak3zGYL23tHxLPIDpx8aMS4sL1+6oZSFrOH3MIH35RLIRM9p/GUxwR0IgQ4Rdc5fWtgWmtaaeRs87pNXXMP1YZx3zIY2avvhmfsmWBDN0YqsmroFNQOex56GoqGNwrmwL+/FsEhz6Pmqn8ivqGck93u2/sje/ltkK86m8826dx2VsoVEd+MAkwj0jsD94T46uQ84+hEJLEoF96ZP4fywTqE7p5N5jtZomS4xC98mWirAiiTBkC5spgc7hQTgX8oosSxEt0Xf3UZEitLYX49Fwnux2TU3DucHPqio9dIGoxShxwYxCYxHjedSKsAtfX7+Sh1DOt4/JvUIfLYnghcfCZU8sMit89E8tt9SCzeiRgd4i9mmMWB/qs3HD85k1yJrJO31jADaQ25EzSA4tPXQltD96malZ6Y7+cY0QWeJ8aQO9Fw+WXlvO7Qdi0hdboTuC19W+bSJ8Ak2X9b95nWYK9GqjYwQQ7pjKyVd5ODvBWxqd8gQSS3tqcK0c+3Iv45K3L+HmRSww6Kjssju0Fun82/1oAV3eHOtaeBmUphJ8bNGrUJCkFFoS7bA3X5XiRmbIQRDluRS7PKktjeRzRZT7juOg9izxSqTHRlWea0Ei7dCa/d1X2OQNoEqO+shn6Qpd2RDVqUQgV7tueARr6XHRSDO6j85lDiH3cGNpcjQYf++nIrJZ4iE4yhyXbwnE9BcFqkFw5HxkNR6BUxmKtVnbsuHWqTX5ZFEShU5F/0iwaqDNcPqYO5TYBV1TdY4larVIu8/zCkTYDayn2HkmeFyiauE06JKzNGr2U9exGx/i2gU3vqxnIkyR9Ut5ZBjBENRiyLyDYbVSSs745gj2cKRsTh7ADuEfoUuCjoK1JUQaL25OGdKIy1D+FbZ0BgMV1307pG9ru4v6mxSomh2H+eAMU+bSB2IOtzVw1qb3sXgXm3HJ8u0QhMrUQzySNDGX0GwA7+hsENB7ZpRt9PlNg2ivzH4tDZ3gcrT5NtQBG8Mi856SgMQCwI8C9oPNZhT67ez2eroTUtk9rYXY3IfXP54FA6ExGgpG9lStuVpC55yHnlWlSyoj4l+1A15K/I+ezWBoqH14e6heUYz4cEILD1jw6R4nHyMJw4rPAg9NQXMH3zQdRSYFrfXQEHDVDf/JvTWqQorX4gq3ab/fb10Fl5jnV7UDvgWVKFqRFBJt9o9+0KVgwktc18Klmttf3JX91VTgOIwkvvX8/TQ9KJtAqQwTmuJ3LeuRmyU0GYyOmKvk8hMfXrVD7Jg8J2FhI3yXGr/cZkSLRd/JFPUTt2GoWQSGXn+5H1xc0Qi9si3Tgl1cUdIzoiZ8WvIRfm8PS7qklvIzjyReir9zXyqWTdLjzYBZMAFxqtU659vRO1Pf+I8MMfgUU2hIFFCHz2S66+TwVOWXl4qUcL5H15N5QxfXi3heavw8FBzyL6q9kwNtRTssPvMDtYsDHNSzBd9/oKbBiryEK9YhpqhkwhPrScq1nloYuR89FNEDqdusJ8aY8H1ofkx5sRuv0tM8hKEF0ueH9+DtwTB5lfPkWIvvgV4vM2Ivsvl0MotKf+JsuMiz6xkJiebnBe05c7/uonW5AgXzP8/gpr+ycp84uK4bzvAjh+VIhTjdNCgByktyKPzUN4ygIkqyNW7gngGtmDAqX9oVzQ1dwNlA5QnDJGQou+tZL81x0ssYOTBcpZhXDePRQOlkXgPD2+2+L0EWAdKNIdm7Me0QfmQN1B/KS1F9fh9/C0Q0e/Iig/7gF5UFGKX6j1PWCxxOoYtMUU9Z+zEYlFNOO+O8DnmsiNJhpEg7tCuWUIHFcVpzXWlwpOPwHWgW2V+Gg9wm+WIPbBaujEVZq0l7mL1pHrh9Se+NDWObzCkkjcqNSeWJV8n5kf6hQPfwMMC/YycnpDJbRS4jw3HUB8dxV0iuMl9lQS9RVEsjbK4/dmzJ7WaK8H3vtHwXl1McSOuaed4Opw+grwSKgGT+tLLliPGB3qpjLOmNSlSpilCnjcwNw4Y9mq5t/NQld136NmfhUX+3vSGhBWO4oEuVc7Isd7QBnVHQ6W2e1PU/jqByAzBHgsEjriX24jk3479BLiQvdWQSujKENZLd9AqlqbuzQIFgeahGF9CYjkVqARa+Mkes3ZrQ2k3vlwjaLwVL92tu9jsAOZKcD6wPQey4lhVQcTBt+xBFbDU7S0HwVckaVwnpJ9dQ7bCfWfgP8cAf4/xelhCzfjhNEswAxHswAzHM0CzHA0CzDD0SzADEezADMczQLMcDQLMMPRLMAMR7MAMxzNAsxwNAsww9EswAxHswAzHP8HGek+ctO2sd8AAAAASUVORK5CYII=';
    }
    if (
      !data.login ||
      !data.username ||
        data.login.length < 1 ||
        data.username.length < 3 ||
        data.username.length > 12 ||
      !/^[a-zA-Z]{3,12}$/.test(data.username)
    )
      return { error: 'bad format' };
    return await this.userService.createUser(data);
  }

  /**
   * return all users
   */
  @Get('get_all')
  @UseGuards(CookieAuthGuard)
  async get_all() {
    return await this.userService.getAllUsers();
  }

  /**
   * get the user using its id
   * @param userId :Number user id
   */
  @Get(':id')
  @UseGuards(CookieAuthGuard)
  async getUser(@Param('id') userId: number) {
    return await this.userService.getSingleUser(userId);
  }

  /**
   * get the user by its login
   * @param login :String user login
   */
  @Get('find/:login')
  async getUser_bylogin(@Param('login') login: string) {
    return await this.userService.getSingleUser_bylogin(login);
  }

  /**
   * get the user by its username
   * @param userName :String username
   */
  @Get('find_username/:username')
  @UseGuards(CookieAuthGuard)
  async getUser_byUserName(@Param('username') userName: string) {
    return await this.userService.getSingleUser_byUserMame(userName);
  }

  /**
   * set the 2fa boolean
   */
  @Patch('set2fa/')
  @UseGuards(CookieAuthGuard)
  async set2fa(@Req() request: Request) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    return await this.userService.set2fa(userLogin);
  }

  /**
   * unset the 2fa boolean
   */
  @Patch('unset2fa/')
  @UseGuards(CookieAuthGuard)
  async unset2fa(@Req() request: Request) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    return await this.userService.unset2fa(userLogin);
  }

  /**
   * Edit the username
   */
  @Post('editusername')
  @UseGuards(CookieAuthGuard)
  async editUsername(
    @Body('new_username') username: string,
    @Req() req: Request,
  ) {
    const userLogin = await this.apiService.getCookieLogin(req);
    if (!userLogin) return { error: 'user not found' };
    if (username.length < 3 || username.length > 12)
      return { error: 'bad username' };
    if (!/^[a-zA-Z]{3,12}$/.test(username)) return { error: 'bad username' };
    return await this.userService.editUsername(req, userLogin, username);
  }

  /**
   * Edit the avatar
   */
  @Post('editavatar')
  @UseGuards(CookieAuthGuard)
  async editAvatar(@Body('new_avatar') avatar: string, @Req() req: Request) {
    const userLogin = await this.apiService.getCookieLogin(req);
    if (!userLogin) return { error: 'user not found' };
    if (!avatar || avatar.length < 1) return {error: "file error"};
    return await this.userService.editAvatar(req, userLogin, avatar);
  }
}
