import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // Accepting same ip for API and client
  //app.use(cors());
  app.use(cookieParser());
  app.enableCors({
    origin: 'http://' + process.env.IP_HOST  + ':' + process.env.FRONT_PORT,
    credentials: true
  });

  await app.listen(process.env.BACK_PORT);
}
bootstrap();
