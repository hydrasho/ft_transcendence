import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    const imgPath =
      "'https://developers.giphy.com/branch/master/static/api-c99e353f761d318322c853c03ebcf21b.gif'";
    return (
      '' +
      '<body style=" background-image: url(' +
      imgPath +
      '); background-size:cover;">' +
      '<center><h1 style="text-align: center; background-color: #ffffff77; width:420px;padding: 20px;border-radius:25px;">' +
      '!!! This is the backend part !!!' +
      '</h1></center>' +
      '</body>'
    );
  }
}
