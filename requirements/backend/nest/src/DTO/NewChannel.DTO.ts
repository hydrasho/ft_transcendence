import { IsNotEmpty, IsString } from 'class-validator';
import { channelDTO } from './channel.DTO';

export class NewChannelDTO {
  @IsNotEmpty()
  channelInfo: channelDTO;

  @IsString()
  @IsNotEmpty()
  userLogin: string;
}
