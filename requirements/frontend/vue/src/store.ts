import {Commit, createStore} from 'vuex'
import createPersistedState from 'vuex-persistedstate'

const persistedState = createPersistedState({
	key: 'my-app',
	storage: window.sessionStorage
  });

export default createStore({
	plugins: [persistedState],
	state: {
		authenticated: false,
		login: '' as string,
		avatar: '' as string,
		id: {} as number,
	},
	mutations: {
		SET_AUTH: (state: { authenticated: boolean }, auth: boolean) => state.authenticated = auth,
		SET_LOGIN: (state: { login: string }, login: string) => state.login = login,
		SET_AVATAR: (state: { avatar: string }, avatar: string) => state.avatar = avatar,
		SET_ID: (state: { id: number }, id: number) => state.id = id,
	},
	actions: {
		setAuth: ({commit}: { commit: Commit }, auth: boolean) => commit('SET_AUTH', auth),
		setLogin: ({commit}: { commit: Commit }, login: string) => commit('SET_LOGIN', login),
		setAvatar: ({commit}: { commit: Commit }, avatar: string) => commit('SET_AVATAR', avatar),
		setId: ({commit}: { commit: Commit }, id: number) => commit('SET_ID', id),

	},
	modules: {}
})