import { GameService } from './game.service';
import { Controller, Get, Param } from '@nestjs/common';

@Controller('game')
export class GameController {
  constructor(private readonly gameService: GameService) {}

  /**
   * return all matchs of a user (Match[])
   * @param userLogin user login
   */
  @Get('get_all/:login')
  async getAllMatches(@Param('login') userLogin: string) {
    const response = await this.gameService.getMatchsOf(userLogin);
    return response;
  }

  /**
   * return all matchs of a user (Match[])
   * @param userLogin user login
   */
  @Get('get_stat/:login')
  async getStat(@Param('login') userLogin: string) {
    const response = await this.gameService.getStatsOf(userLogin);
    return response;
  }
}
