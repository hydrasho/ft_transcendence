import { Module } from '@nestjs/common';
import { FriendController } from './friends.controller';
import { FriendsService } from './friends.service';
import { UsersController } from '../users/users.controller';
import { UsersService } from '../users/users.service';
import { UsersModule } from '../users/users.module';
import { AppModule } from '../app.module';

@Module({
  imports: [AppModule, UsersModule], // New modules must be imported here
  controllers: [FriendController, UsersController],
  providers: [FriendsService, UsersService],
})
export class FriendsModule {}
