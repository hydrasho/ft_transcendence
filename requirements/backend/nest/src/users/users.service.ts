import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { User } from '../database/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindManyOptions } from 'typeorm';
import { UserFormat } from '../database/models/user.model';
import { Request } from 'express';
import { SocketsService } from 'src/sockets/sockets.service';
import {UserDTO} from "../DTO/UserDTO";

/** Telling NestJS that it could inject this class into another one **/
@Injectable()
export class UsersService {
  /** Initialize attributes **/

  constructor(
    @InjectRepository(User) readonly UserRepository: Repository<User>,
    private readonly socketsService: SocketsService,
  ) {}

  // METHODS ************************************ //

  /**
   * insert a user to the DB
   * @param data :UserFormat user row
   */
  async createUser(data: UserDTO): Promise<User> {
    const result = await this.UserRepository.save(data).catch(() => {
      throw new BadRequestException('login or username already in use');
    });

    return result;
  }

  /**
   * returns all users from the DB
   */
  async getAllUsers() {
    const users = await this.UserRepository.find();

    return users;
  }

  /**
   * search a user with its id
   * @param userId :Number user id
   */
  async getSingleUser(userId: number) {
    const user = await this.UserRepository.findOne({
      where: { id: userId },
    } as FindManyOptions<User>);

    if (user !== null) {
      // const userDTO = plainToClass(UserDTO, user);
      return user;
    } else {
      return { error: 'user not found' };
    }
  }

  /**
   * search a user with its login
   * @param login :String user login
   */
  async getSingleUser_bylogin(login: string) {
    const user = await this.UserRepository.findOne({
      where: { login: login },
    }).catch(() => {
      return { error: 'user not found' };
    });

    if (user !== null && user instanceof User) {
      return user;
    } else {
      return { error: 'user not found' };
    }
  }

  /**
   * search a user with its username
   * @param UserName :String username
   */
  async getSingleUser_byUserMame(UserName: string) {
    const user = await this.UserRepository.findOne({
      where: { username: UserName },
    } as FindManyOptions<User>).catch(() => {
      return { error: 'user not found' };
    });

    if (!(user instanceof User)) return user;
    if (user !== null) {
      return user;
    } else {
      return { error: 'user not found' };
    }
  }

  /**
   * set the 2fa boolean to true if it s possible
   * @param login :String user login
   */
  async set2fa(login: string) {
    const user = await this.getSingleUser_bylogin(login);

    if (user === null || !(user instanceof User)) {
      return { error: 'user not found' };
    }

    if (user.using_2fa) {
      throw new BadRequestException('2fa already set');
    }

    user.using_2fa = true;
    const edit = await this.UserRepository.save(user);

    if (edit === null) {
      throw new InternalServerErrorException('impossible to edit the user');
    }

    return edit;
  }

  /**
   * set the 2fa boolean to false if it s possible
   * @param login :String user login
   */
  async unset2fa(login: string) {
    const user = await this.getSingleUser_bylogin(login);

    if (user === null || !(user instanceof User)) {
      return { error: 'user not found' };
    }

    if (user.using_2fa === false) {
      throw new BadRequestException('2fa already unset');
    }

    user.using_2fa = false;
    const edit = await this.UserRepository.save(user);

    if (edit === null) {
      throw new InternalServerErrorException('impossible to edit the user');
    }

    return edit;
  }

  /**
   * Edit the Username
   * @param req
   * @param login :String user login
   * @param username :String username queried
   */
  async editUsername(req: Request, login: string, username: string) {
    const user = await this.getSingleUser_bylogin(login);
    const user_query_un = await this.UserRepository.findOne({
      where: { username: username },
    } as FindManyOptions<User>).catch(() => {
      return { error: 'user not found' };
    });

    if (user_query_un instanceof User) return { error: 'user already use' };
    if (user === null || !(user instanceof User)) {
      return { error: 'user not found' };
    } else if (!/^[a-zA-Z]{3,12}$/.test(username)) {
      throw new BadRequestException('Bad username format');
    } else if (user_query_un !== null) {
      throw new BadRequestException('Already used username');
    } else {
      user.username = username;
    }

    const edit = await this.UserRepository.save(user);

    if (edit === null) {
      throw new InternalServerErrorException('Impossible to edit the user');
    }
    return { statusText: 'ok' };
  }

  /**
   * Edit the Avatar
   * @param req
   * @param login :String user login
   * @param avatar :String avatar queried
   */
  async editAvatar(req: Request, login: string, avatar: string) {
    const user = await this.getSingleUser_bylogin(login);

    if (user === null || !(user instanceof User)) {
      return { error: 'user not found' };
    } else {
      user.avatar_file = avatar;
    }

    const edit = await this.UserRepository.save(user);

    if (edit === null) {
      throw new InternalServerErrorException('Impossible to edit the user');
    }
    return { statusText: 'ok' };
  }
}
