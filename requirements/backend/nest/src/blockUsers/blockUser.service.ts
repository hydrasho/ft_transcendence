import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { FindManyOptions, Repository } from 'typeorm';
import { blockUser } from '../database/entities/blockUser.entity';

@Injectable()
export class BlockUserService {
  constructor(
    @InjectRepository(User) readonly UserRepository: Repository<User>,
    @InjectRepository(blockUser)
    readonly blockUserRepository: Repository<blockUser>,
  ) {}

  // METHODS ************************************ //

  /**
   * insert the block relation to the db
   * @param data :blockUser block row
   */
  async createBlock(data: blockUser) {
    const blockRow = this.blockUserRepository.save(data).catch(() => {
      throw new BadRequestException('Error the row already exists');
    });
    return blockRow;
  }

  /**
   * remove a block row from the DB
   * @param blockRowId :Number id of the row
   */
  async deleteRow(blockRowId: number) {
    return await this.blockUserRepository.delete(blockRowId).catch(() => {
      return { error: 'Error relation does not exist' };
    });
  }

  /**
   * return all block relation (dangerous)
   */
  async getAllBlockRow() {
    const blockRow = await this.blockUserRepository
      .find({
        relations: ['blocker', 'blocked'],
      })
      .catch(() => {
        return { error: 'Error relation does not exist' };
      });
    return blockRow;
  }

  /**
   * return an array of users that are blocked by the user
   * @param login :String user login
   */
  async getUsersBlocked(login: string) {
    const user = await this.UserRepository.findOne({
      where: { login: login },
    } as FindManyOptions<User>).catch(() => {
      return { error: 'user not found' };
    });

    if (user === null) {
      return { error: 'user not found' };
    }

    const blocked = await this.blockUserRepository
      .find({
        relations: { blocker: true, blocked: true },
        where: [{ blocker: { login: login } }],
      } as FindManyOptions<blockUser>)
      .catch(() => {
        return { error: 'blocked not found' };
      });

    if (!Array.isArray(blocked)) return blocked;

    if (blocked.length < 1) return { error: 'blocked not found' };

    const users_blocked = blocked.map((relation) => {
      return relation.blocked;
    });

    return users_blocked;
  }

  /**
   * return an array of users that have blocked the user
   * @param login :String user login
   */
  async getUsersBlocker(login: string) {
    const user = await this.UserRepository.findOne({
      where: { login: login },
    } as FindManyOptions<User>).catch(() => {
      return { error: 'user not found' };
    });

    if (user === null) {
      return { error: 'user not found' };
    }

    const blockers = await this.blockUserRepository
      .find({
        relations: { blocker: true, blocked: true },
        where: [{ blocked: { login: login } }],
      } as FindManyOptions<blockUser>)
      .catch(() => {
        return { error: 'blockers not found' };
      });

    if (!Array.isArray(blockers)) return blockers;

    if (blockers.length < 1) return { error: 'blockers not found' };

    const users_blocker = blockers.map((relation) => {
      return relation.blocker;
    });

    return users_blocker;
  }

  /**
   * search if a block relation already exists
   * @param blockerLogin :String blocker login
   * @param blockedLogin :String blocked login
   */
  async getRelation(blockerLogin: string, blockedLogin: string) {
    const relation1 = await this.blockUserRepository
      .findOne({
        where: [
          {
            blocker: { login: blockerLogin },
            blocked: { login: blockedLogin },
          },
        ],
      })
      .catch(() => {
        return { error: 'relation not found' };
      });

    if (relation1 !== null) return relation1;
    return null;
  }
}
