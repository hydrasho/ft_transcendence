import { Module } from '@nestjs/common';
import { AppModule } from '../app.module';
import { channelMemberService } from './channelMember.service';
import { channelMemberController } from './channelMember.controller';

@Module({
  imports: [AppModule], // New modules must be imported here
  controllers: [channelMemberController],
  providers: [channelMemberService],
})
export class channelMemberModule {}
