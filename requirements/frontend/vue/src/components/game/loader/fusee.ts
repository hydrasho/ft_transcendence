
export class Fusee {
	x: number;
	y: number;
	n: number;
	image: HTMLImageElement;

	constructor(img: string) {
		this.image = new Image();
		this.image.src = img;
		this.n = 0;
		this.x = -42;
		this.y = -42;
	}

	draw(ctx: CanvasRenderingContext2D, wobbly: boolean) {
		if (wobbly){
			if (this.n < 40)
				this.y +=1;
			else
				this.y -=1;
			if (this.n == 80)
				this.n = 0;
			this.n++;
		}
		setShadow(ctx, "rgba(30, 30, 30, 0.9)", 42);
		ctx?.drawImage(this.image, this.x, this.y);
		endShadow(ctx);
	}
}

function setShadow(ctx: CanvasRenderingContext2D, color: string, force: number) {
	ctx.shadowColor = color;
	ctx.shadowBlur = force;
	ctx.beginPath();
	ctx.fill();
}

function endShadow(ctx: CanvasRenderingContext2D) {
	ctx.shadowColor = 'transparent';
	ctx.shadowBlur = 0;
}
