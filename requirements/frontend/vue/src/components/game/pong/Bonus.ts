
interface Vector2i{
	x: number;
	y: number;
}

interface Rect {
	x: number;
	y: number;
	w: number;
	h: number;
}


export class Bonus{
	r: Rect;
	red : number;
	green: number;
	blue: number;
	image: HTMLImageElement;

	constructor(img: string) {
		this.image = new Image();
		this.image.src = img;
		this.r = {x: 0, y: 0, w: this.image.width, h: this.image.height};
		this.red = 255;
		this.green = 1;
		this.blue = 1;
	}

	draw(ctx: CanvasRenderingContext2D, ratio: number, shift: Vector2i) {
		this.color();
		ctx.fillStyle = `rgba(${this.red},${this.green},${this.blue}, 0.9)`;
		ctx.fillRect((this.r.x + shift.x) * ratio, (this.r.y + shift.y) * ratio, this.r.w * ratio, this.r.h * ratio);
		ctx.drawImage(this.image, (this.r.x + shift.x) * ratio, (this.r.y + shift.y) * ratio, this.r.w * ratio, this.r.h * ratio);
	}

	color(){
		if (this.red > 1 && this.blue == 1){
			this.red -= 2;
			this.green += 2;
		}
		if (this.green > 1 && this.red == 1){
			this.green -= 2;
			this.blue += 2;
		}
		if (this.blue > 1 && this.green == 1){
			this.red += 2;
			this.blue -= 2;
		}
	}
}
