import {
  BeforeInsert,
  BeforeUpdate,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity('Friends')
export class Friends {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.id, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn()
  friend_1: User;

  @ManyToOne(() => User, (user) => user.id, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn()
  friend_2: User;

  @BeforeInsert()
  @BeforeUpdate()
  checkColumnEquality() {
    if (this.friend_1 === this.friend_2)
      throw new Error('friend_1 and friend_2 must be different');
  }
}
