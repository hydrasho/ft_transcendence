import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';
import { channel } from './channel.entity';
import { MemberType } from '../enums/MemberType.enum';

@Entity('channelMember')
export class channelMember {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => channel, (channel) => channel.id, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn()
  channel: channel;

  @ManyToOne(() => User, (user) => user.login, { nullable: false })
  @JoinColumn()
  user: User;

  @Column({
    type: 'enum',
    enum: MemberType,
    default: MemberType.NORMAL,
  })
  type: MemberType;

  @Column({ nullable: false })
  register: boolean;

  @Column({ nullable: false })
  muted: boolean;

  @Column({ nullable: false })
  blocked: boolean;

  @Column({ nullable: true })
  end_mute: Date; // mettre la date
}
