import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/database/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { isJWT } from 'class-validator';
import { Socket } from 'socket.io';

@Injectable()
export class ApiService {
  constructor(
    private jwtService: JwtService,
    private userService: UsersService,
  ) {}

  /**
   * verify the validity of the jwt cookie
   * @param request
   * @param login
   */
  async cookie_checkLogin(request: Request, login: string): Promise<boolean> {
    const cookie = request.cookies['jwt'];

    if (!isJWT(cookie)) return false;
    if (cookie) {
      const data = await this.jwtService.verifyAsync(cookie);
      const user = await this.userService.getSingleUser(data['id']);

      if (!data || !(user instanceof User) || user.login !== login)
        throw new UnauthorizedException(
          'Invalid credentials, your are not allowed to do this.',
        );
    }
    return true;
  }

  /**
   * return the user login according to the jwt cookie
   * @param request
   */
  async getCookieLogin(request: Request): Promise<string> {
    const cookie = request.cookies['jwt'];

    if (!isJWT(cookie)) return null;
    if (cookie) {
      const data = await this.jwtService.verifyAsync(cookie).catch(() => {
        return null;
      });
      if (!data) return null;
      return data['login'];
    }
    return null;
  }

  /**
   * get the user login according to the socket
   * @param client
   */
  async getCookieLoginSocket(client: Socket): Promise<string> {
    const cookies = this.splitCookiesSocket(client.handshake.headers.cookie);

    if (cookies.size && cookies.has('jwt')) {
      const data = await this.jwtService
        .verifyAsync(cookies.get('jwt'))
        .catch(() => {
          return null;
        });
      if (data) return data['login']; // a voir avec thibaud je suis pas sur de ca
    }
    return null;
  }

  /**
   * extract cookie from sockets
   * @param cookies
   * @private
   */
  private splitCookiesSocket(cookies: string): Map<string, string> {
    const cookieSplit = cookies && cookies.split(';');
    const cookieMap = new Map();

    if (cookieSplit) {
      cookieSplit.map((cookie: string) => {
        const tmp = cookie.split('=');
        const key = tmp[0];
        const value = tmp[1];

        cookieMap.set(key.trim(), value.trim());
      });
    }
    return cookieMap;
  }
}
