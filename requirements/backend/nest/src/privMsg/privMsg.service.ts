import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { privMsg } from '../database/entities/privMsg.entity';
import { blockUser } from '../database/entities/blockUser.entity';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';

@Injectable()
@WebSocketGateway({ cors: { origin: 'http://' + process.env.IP_HOST + ':' + process.env.FRONT_PORT, credentials: true } })
export class privMsgService {
  @WebSocketServer() server: Server;

  constructor(
    @InjectRepository(privMsg) readonly privMsgRepository: Repository<privMsg>,
    @InjectRepository(blockUser)
    readonly blockUserRepository: Repository<blockUser>,
  ) {}

  // METHODS ************************************ //

  /**
   * add the new private message to the database
   */
  async create(messageInfo: privMsg) {
    const resp = await this.privMsgRepository.save(messageInfo).catch(() => {
      return { error: 'db error' };
    });
    this.newMessageEvent(messageInfo.receiver.login, messageInfo.sender);
    this.newMessageEvent(messageInfo.sender.login, messageInfo.receiver);
    return resp;
  }

  /**
   * get the users list that have a discussion with the user
   */
  async getDiscussionList(login: string) {
    const discussions = await this.privMsgRepository
      .find({
        relations: { sender: true, receiver: true },
        where: [{ sender: { login: login } }, { receiver: { login: login } }],
      } as FindManyOptions<privMsg>)
      .catch(() => {
        return { error: 'no discussion found' };
      });

    if (!Array.isArray(discussions)) return discussions;

    if (discussions.length < 1) return { error: 'no discussion found' };

    const discussionList = discussions.map((disc) => {
      if (disc.sender.login === login) {
        return disc.receiver;
      } else {
        return disc.sender;
      }
    });

    const uniqueUsers = [];
    const seenUsers = new Set();

    for (const user of discussionList) {
      if (!seenUsers.has(user.login)) {
        seenUsers.add(user.login);
        uniqueUsers.push(user);
      }
    }

    return uniqueUsers;
  }

  /**
   * get all messages between login1 and login2
   */
  async getDiscussionWith(login1: string, login2: string) {
    const messageWithL1 = await this.privMsgRepository
      .find({
        relations: { sender: true, receiver: true },
        where: [{ sender: { login: login1 } }, { receiver: { login: login1 } }],
      } as FindManyOptions<privMsg>)
      .catch(() => {
        return { error: 'no discussion found' };
      });

    if (!Array.isArray(messageWithL1)) return messageWithL1;

    if (messageWithL1.length < 1) return { error: 'no discussion found' };

    const discussion: privMsg[] = [];

    for (const msg of messageWithL1) {
      if (msg.sender.login === login2 || msg.receiver.login === login2) {
        discussion.push(msg);
      }
    }

    if (discussion.length < 1) return { error: 'no discussion found' };
    return discussion;
  }

  // SOCKETS ************************************ //

  /**
   * emit the action to the socket
   * @param receiverLogin
   * @param sender
   */
  newMessageEvent(receiverLogin: string, sender: any) {
    this.server.emit(receiverLogin, sender);
  }
}
