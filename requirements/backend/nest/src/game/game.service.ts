import { Injectable } from '@nestjs/common';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { LobbyInfo } from 'src/game_lobby/game_lobby.service';
import { Server } from 'socket.io';
import { timer } from 'rxjs';
import { EmitMatchType } from './game.gateway';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/database/entities/user.entity';
import { FindManyOptions, Repository } from 'typeorm';
import { Match } from 'src/database/entities/match.entity';
import { Rect } from './game.type';
import { isArray } from 'class-validator';
import { Game } from './Game';


@Injectable()
@WebSocketGateway({ cors: {origin: 'http://' + process.env.IP_HOST + ':' + process.env.FRONT_PORT, credentials: true}, namespace: '/game' })
export class GameService {
  constructor(
    @InjectRepository(Match) readonly matchRepository: Repository<Match>,
    @InjectRepository(User) readonly usersRepository: Repository<User>,
  ) { }

  /**
   * @attribute _server       : Socket server
   * @attribute _currentGames : Map< key: matchID, value: associated Game>
   * @attribute _loginSocket  : Map< key: login, value: associated socketID>
   */
  @WebSocketServer() _server: Server;
  private _currentGames: Map<string, Game> = new Map();
  private _loginSocket: Map<string, string> = new Map();

  //     .M"""bgtiond                  `7MM               mm              //
  //    ,MI    "Y                    MM               MM              //
  //    `MMb.      ,pW"Wq.   ,p6"bo  MM  ,MP'.gP"Ya mmMMmm ,pP"Ybd    //
  //      `YMMNq. 6W'   `Wb 6M'  OO  MM ;Y  ,M'   Yb  MM   8I   `"    //
  //    .     `MM 8M     M8 8M       MM;Mm  8M""""""  MM   `YMMMa.    //
  //    Mb     dM YA.   ,A9 YM.    , MM `Mb.YM.    ,  MM   L.   I8    //
  //    P"Ybmmd"   `Ybmd9'   YMbmd'.JMML. YA.`Mbmmd'  `MbmoM9mmmP'    //
  //                                                                  //
  //                Section about sockets management                  //

  newConnexion( socketId: string, login: string, queryMatchID: string,): { success: boolean; message: string } {
    let matchID = this.getGameIdByLogin(login);

    if (matchID) {
      if (matchID !== queryMatchID) {
        return { success: false, message: 'Not in that game' };
      }

      let match = this._currentGames.get(matchID);

      // In case of reconnexion
      if (match.p1 && match.p1.login === login && match.p1.away)
        match.p1.away = false;
      else if (match.p2 && match.p2.login === login && match.p2.away)
        match.p2.away = false;
	  
      // Update or set the new socketID
      this._loginSocket.set(login, socketId);

      // Send players info
      this.sendPlayersInfo(matchID, match);

      // Countdown to synchronize screens
      this.countdownSynchronize(matchID, match);
    }
	else
		return { success: false, message: 'In any game.' };
    return { success: true, message: 'Success.' };
  }

  newDisconnexion(login: string) {
    this._loginSocket.delete(login);
    let matchID = this.getGameIdByLogin(login);

    if (matchID) {
     let match = this._currentGames.get(matchID);

      // Protection in case of both p1/ p2 away
      if (match.p1.login === login)
		  match.p1.away = true;
      else if (match.p2.login === login)
		  match.p2.away = true;

      if (match.p1.away && match.p1.discoAlready)
        this.endGame(match, 2, matchID);
      else if (match.p2.away && match.p2.discoAlready)
        this.endGame(match, 1, matchID);
      else if (match.p1.away && match.p2.away) {
        if (match.p1.score === match.p2.score)
          this.endGame(match, match.p1.login === login ? 1 : 2, matchID);
        else
          this.endGame(match, match.p1.score > match.p2.score ? 1 : 2, matchID);
      }

      if (match.p1.away) match.p1.discoAlready = true;
      else if (match.p2.away) match.p2.discoAlready = true;

      // Launch subscription to check user reconnection
      //      and stop sending ball position
      match.timerSub.unsubscribe();
      match.timerSub = timer(0, 1000).subscribe((iteration: number) => {
        if (iteration === 10) {
          if (match.p1.away)
			  this.endGame(match, 2, matchID);
          else if (match.p2.away)
			  this.endGame(match, 1, matchID);
          match.timerSub.unsubscribe();
        }
      });
    }
  }

  private countdownSynchronize(matchID: string, match: Game) {
    if (match.timerSub !== undefined)
		match.timerSub.unsubscribe();
    // Suscribe timer to send countdown
		match.timerSub = timer(0, 500).subscribe((iteration: number) => {
      // When at the end of counter, reset subscription to be ball position
      if (iteration === 10) {
        this.notifyPlayers(matchID);
        this.initSendBallPosSub(matchID, match);
      }
	  else {
        this._server.emit(matchID, {
          type: EmitMatchType.PLAYER2,
          data: match.p2.rect,
        });
        this._server.emit(matchID, {
          type: EmitMatchType.PLAYER1,
          data: match.p1.rect,
        });
		if (match.bonus !== undefined)
			this._server.emit(matchID + ' bonus', { x: match.bonus.r.x, y: match.bonus.r.y });
        this._server.emit( matchID + ' COUNTDOWN', (10 - iteration - (iteration % 2)) / 2);
        this._server.emit(matchID + ' BALL', {
				pos: { x: match.ball.r.x, y: match.ball.r.y },
				color: match.ball.color,
        });
      }
    });
  }

  getSocketByLogin(login: string): string | undefined {
    return this._loginSocket.get(login);
  }

  getGameIdByLogin(login: string): string | undefined {
    const gameIDs: string[] = [];

    this._currentGames.forEach((matchInfo: Game, matchID: string) => {
      if (matchInfo.p1.login == login || matchInfo.p2.login == login)
        gameIDs.push(matchID);
    });
    if (gameIDs.length === 0) return undefined;
    return gameIDs[0];
  }

  //    `7MMM.     ,MMF'         mm         `7MM                      //
  //      MMMb    dPMM           MM           MM                      //
  //      M YM   ,M MM   ,6"Yb.mmMMmm ,p6"bo  MMpMMMb.  ,pP"Ybd       //
  //      M  Mb  M' MM  8)   MM  MM  6M'  OO  MM    MM  8I   `"       //
  //      M  YM.P'  MM   ,pm9MM  MM  8M       MM    MM  `YMMMa.       //
  //      M  `YM'   MM  8M   MM  MM  YM.    , MM    MM  L.   I8       //
  //    .JML. `'  .JMML.`Moo9^Yo.`MbmoYMbmd'.JMML  JMML.M9mmmP'       //
  //                                                                  //
  //              Section about matchs management                     //

  newGame(lobby: LobbyInfo) {
    let newMatch = new Game(lobby.p1.login, lobby.p2.login, lobby.gameID.startsWith('custom_'));
	
	this.initSendBallPosSub(lobby.gameID, newMatch);
    // Init in map the created element
    this._currentGames.set(lobby.gameID, {...newMatch});
  }

  private async endGame(match: Game, winner: 1 | 2, matchID: string) {
    // Send winner signal
    this._server.emit(matchID + ' END', winner);

    // Update DB
    const player1 = await this.usersRepository.findOneBy({
      login: match.p1.login,
    });
    const player2 = await this.usersRepository.findOneBy({
      login: match.p2.login,
    });

    const newMatch: Match = {
      id: matchID,
      timestamp: new Date(),
      left_player: player1,
      right_player: player2,
      left_player_score: match.p1.score,
      right_player_score: match.p2.score,
      is_left_winner: winner === 1,
    };

    await this.matchRepository.save(newMatch);
    this._currentGames.delete(matchID);
  }

  /**
   * Send informations about Player: avatar and username
   * * @param matchID gameID of desired match
   * @param match   Game associated with MatchID
   */
  private async sendPlayersInfo(matchID: string, match: Game) {
    const player1 = await this.usersRepository.findOneBy({
      login: match.p1.login,
    });
    const player2 = await this.usersRepository.findOneBy({
      login: match.p2.login,
    });

    this._server.emit(matchID + ' PLAYERSINFO', {
      p1: {
        username: player1.username,
        avatar: player1.avatar_file,
      },
      p2: {
        username: player2.username,
        avatar: player2.avatar_file,
      },
    });
  }

  /**
   * Send id in game and scores
   *
   * @param gameID gameID of desired match
   * @returns gameID found and notified
   */
  notifyPlayers(gameID: string): boolean {
    const match = this._currentGames.get(gameID);

    if (match === undefined)
		return false;

    const socketP1 = this.getSocketByLogin(match.p1.login);
    const socketP2 = this.getSocketByLogin(match.p2.login);

    if (socketP1 === undefined || socketP2 === undefined) return false;
    this._server.emit(socketP1 + ' game', {
      id: 1,
      scoreP1: match.p1.score,
      scoreP2: match.p2.score,
    });
    this._server.emit(socketP2 + ' game', {
      id: 2,
      scoreP1: match.p1.score,
      scoreP2: match.p2.score,
    });
    return true;
  }

  /**
   * Update players information in _currentGames also check ball collision
   *
   * * @param matchID gameID of desired match
   * @param type    enum for which player
   * @param value   Rect of the new player position
   */
  updatePlayerPosition(matchID: string, type: EmitMatchType, value: Rect) {
    if (this._currentGames.has(matchID)) {
      const match = this._currentGames.get(matchID);

      if (type === EmitMatchType.PLAYER1) {
        match.p1.rect = value;
      } else if (type === EmitMatchType.PLAYER2) {
        match.p2.rect = value;
      }
    }
  }

  private initSendBallPosSub(matchID: string, match: Game) {
    // Unsubscription protection
    if (match.timerSub !== undefined)
		match.timerSub.unsubscribe();

    // Suscribe to send every 13 ms ball/ bonuses position
    match.timerSub = timer(0, 13).subscribe((iteration: number) => {
      this.sendBallPosition(matchID, match);
      if (matchID.startsWith('custom_')) {
        this.sendBonusPosition(matchID, match, iteration);
      }
    });
  }

  private sendBallPosition(matchID: string, match: Game) {
    const playerPoint: number = match.ball.iterate(
      match.p1.rect,
      match.p2.rect,
    );

    this._server.emit(matchID + ' BALL', {
      pos: { x: match.ball.r.x, y: match.ball.r.y },
      color: match.ball.color,
    });

    if (playerPoint !== 0) {
      if (playerPoint === 1)
		  match.p1.score++;
      else
		  match.p2.score++;
      this._server.emit(matchID + ' SCORE', playerPoint);

      if (match.p1.score === 10 || match.p2.score === 10) {
        if (match.p1.score === 10)
			this.endGame(match, 1, matchID);
        else
			this.endGame(match, 2, matchID);
        match.timerSub.unsubscribe();
      }
    }
  }

  //      .g8"""bgd                      mm                                //
  //    .dP'     `M                      MM                                //
  //    dM'       ``7MM  `7MM  ,pP"Ybd mmMMmm ,pW"Wq.`7MMpMMMb.pMMMb.      //
  //    MM           MM    MM  8I   `"   MM  6W'   `Wb MM    MM    MM      //
  //    MM.          MM    MM  `YMMMa.   MM  8M     M8 MM    MM    MM      //
  //    `Mb.     ,'  MM    MM  L.   I8   MM  YA.   ,A9 MM    MM    MM      //
  //      `"bmmmd'   `Mbod"YML.M9mmmP'   `Mbmo`Ybmd9'.JMML  JMML  JMML.    //
  //                                                                       //
  //                 Methods used for custom mode matchs                   //

  private sendBonusPosition(
    matchID: string,
    match: Game,
    iteration: number,
  ) {
    if (match.bonus.is_collision(match.ball.r))
      this.magik_effect(match, iteration, match.ball.color);
    this._server.emit(matchID + ' bonus', { x: match.bonus.r.x, y: match.bonus.r.y });

    // Player 1 bonus bar
    if (match.bonus.time_p1 > 0) {
      if (match.bonus.time_p1 + 700 >= iteration) {
        if (match.p1.rect.h <= 70) {
          match.p1.rect.h++;
          if (match.p1.rect.y + match.p1.rect.h < 242)
			  match.p1.rect.y--;
          if (match.p1.rect.y < 20)
			  match.p1.rect.y++;
          this._server.emit(matchID, {
            type: EmitMatchType.PLAYER1,
            data: match.p1.rect,
          });
        }
      } else {
        match.p1.rect.h--;
        this._server.emit(matchID, {
          type: EmitMatchType.PLAYER1,
          data: match.p1.rect,
        });
        if (match.p1.rect.h === 40)
			match.bonus.time_p1 = 0;
      }
    }

    // Player 2 match.bonus bar
    if (match.bonus.time_p2 > 0) {
      if (match.bonus.time_p2 + 700 >= iteration) {
        if (match.p2.rect.h <= 70) {
          match.p2.rect.h++;
          if (match.p2.rect.y + match.p2.rect.h < 242)
			  match.p2.rect.y--;
          if (match.p2.rect.y < 20)
			  match.p2.rect.y++;
          this._server.emit(matchID, {
            type: EmitMatchType.PLAYER2,
            data: match.p2.rect,
          });
        }
      }
	  else {
        match.p2.rect.h--;
        this._server.emit(matchID, {
          type: EmitMatchType.PLAYER2,
          data: match.p2.rect,
        });
        if (match.p2.rect.h === 40)
			match.bonus.time_p2 = 0;
      }
    }
  }

  private magik_effect(match: Game, iteration: number, color: string) {
	// Ball is White
    if (color == 'rgba(40,40,40,0.5)') return;

    const r = Math.round(Math.random() * 3);

    if (r <= 1) {
      if (color === 'orange')
		  match.bonus.time_p1 = iteration;
      if (color === 'blue')
		  match.bonus.time_p2 = iteration;
    }
	else if (r === 2) {
      match.ball.speed += 1;
    }
	else if (r === 3) {
      match.ball.speed += 2;
    }
    match.bonus.r.x = 0;
    match.bonus.r.y = 0;
  }

  //   `7MMF'  `7MMF'MMP""MM""YMM MMP""MM""YMM `7MM"""Mq.   //
  //     MM      MM  P'   MM   `7 P'   MM   `7   MM   `MM.  //
  //     MM      MM       MM           MM        MM   ,M9   //
  //     MMmmmmmmMM       MM           MM        MMmmdM9    //
  //     MM      MM       MM           MM        MM         //
  //     MM      MM       MM           MM        MM         //
  //   .JMML.  .JMML.   .JMML.       .JMML.    .JMML.       //
  //                                                        //
  //        Section about HTTP Requests management          //

  /**
   * return an array of matchs' user
   * @param login :String user login
   */
  async getMatchsOf(login: string): Promise<Match[] | { error: string }> {
    const user = await this.usersRepository.findOne({
      where: { login: login },
    } as FindManyOptions<User>);

    if (user === null) {
      return { error: 'User not found' };
    }

    const matchs = await this.matchRepository.find({
      relations: { right_player: true, left_player: true },
      where: [
        { right_player: { login: login } },
        { left_player: { login: login } },
      ],
    } as FindManyOptions<Match>);

    return matchs;
  }

  /**
   * return an object of match stats
   * @param login :String user login
   */
  async getStatsOf( login: string,): Promise< { wins: number; loses: number; score: number } | { error: string } > {
    const matches = await this.getMatchsOf(login);

    if (!isArray(matches))
		return matches;
    else if (matches.length === 0)
		return { error: 'User has played no match' };

    const wins_count = matches.filter((match: Match) => {
      if (match.left_player.login === login)
		  return match.is_left_winner;
      else
		  return !match.is_left_winner;
    }).length;
    const loses_count = matches.length - wins_count;

    const disconnection_count = matches.filter((match: Match) => {
      if (match.left_player_score !== 10 && match.right_player_score !== 10) {
        if (match.left_player.login === login)
			return !match.is_left_winner;
        else
			return match.is_left_winner;
      }
      return false;
    }).length;

    return {
      wins: wins_count,
      loses: loses_count,
      score: disconnection_count * -1 + loses_count * -1 + wins_count,
    };
  }
}
