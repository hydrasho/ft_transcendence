

export class Flash {
	image: HTMLImageElement;
	image_plus: HTMLImageElement;
	is_flash: boolean;
	x: number;
	y: number;
	what: boolean;
	opacity: number;
	up_anime: number;
	xalign: number;
	yalign: number;

	constructor(what: boolean, img: string, img_plus: string, x: number, y: number) {
		this.image = new Image();
		this.image_plus = new Image();
		this.image.src = img;
		this.image_plus.src = img_plus;
		this.what = what;
		this.x = x;
		this.y = y;
		this.is_flash = false;
		this.opacity = 0;
		this.up_anime = 0;
		this.xalign = 0;
		this.yalign = 0;
	}
	
	flash() {
		this.is_flash = true;
		this.up_anime = 0;
		this.xalign = Math.random() * 120 + 20;
		this.yalign = 150; 
		this.opacity = 0;
	}

	private anime_flash() {
		if (this.is_flash == true) {
			this.opacity += 10;
			if (this.opacity >= 100) {
				this.is_flash = false;
			}
		}else if (this.opacity > 0){
			this.opacity -= 10;
		}
	}
	
	private anime_victory() {
		if (this.is_flash == true) {
			this.opacity += 10;
			if (this.opacity >= 100) {
				this.is_flash = false;
			}
		}else if (this.opacity > 0){
			this.opacity -= 1;
		}
	}

	draw(ctx: CanvasRenderingContext2D, ratio: number) {
		if (this.what == false) {
			this.anime_flash();
			if (this.opacity != 0)
			{
				ctx.globalAlpha = (this.opacity / 100);
				ctx.drawImage(this.image,
					this.x * ratio,
					this.y * ratio,
					this.image.width * ratio,
					this.image.height * ratio);
				ctx.globalAlpha = 1;
			}
		}
		if (this.what == true) {
			this.anime_victory();
			if (this.opacity != 0)
			{
				ctx.globalAlpha = (this.opacity / 100);
				ctx.drawImage(this.image,
					this.x * ratio,
					this.y * ratio,
					this.image.width * ratio,
					this.image.height * ratio);
				ctx.drawImage(this.image_plus,
					(this.x + this.xalign) * ratio,
					(this.y + this.yalign - this.up_anime) * ratio,
					this.image_plus.width * ratio,
					this.image_plus.height * ratio);
				this.up_anime++;
				ctx.globalAlpha = 1;
			}
		}
	}
}
