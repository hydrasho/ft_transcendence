import { Module } from '@nestjs/common';
import { AppModule } from '../app.module';
import { privMsgService } from './privMsg.service';
import { privMsgController } from './privMsg.controller';

@Module({
  imports: [AppModule], // New modules must be imported here
  controllers: [privMsgController],
  providers: [privMsgService],
})
export class privMsgModule {}
