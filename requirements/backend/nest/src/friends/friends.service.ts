import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { FindManyOptions, Repository } from 'typeorm';
import { Friends } from '../database/entities/friends.entity';
import { ConnexionStatus, SocketsService } from 'src/sockets/sockets.service';

@Injectable()
export class FriendsService {
  constructor(
    @InjectRepository(User) readonly UserRepository: Repository<User>,
    @InjectRepository(Friends) readonly FriendsRepository: Repository<Friends>,
    public readonly socketService: SocketsService,
  ) {}

  /**
   * insert to the db the Friendship passed in argument
   * @param data :Friends friendship
   */
  async createFriends(data: Friends) {
    const friends = this.FriendsRepository.save(data).catch(() => {
      throw new BadRequestException('Error relation already exist');
    });
    return friends;
  }

  /**
   * remove from the DB the friendship
   * @param relationId :Number id of the relationship
   */
  async deleteFriendShip(relationId: number) {
    return await this.FriendsRepository.delete(relationId).catch(() => {
      return { error: 'the relation does not exist' };
    });
  }

  /**
   * return all friendships (dangerous)
   */
  async getAllFriends() {
    const friends = await this.FriendsRepository.find({
      relations: ['friend_1', 'friend_2'],
    }).catch(() => {
      return { error: 'relation not found' };
    });
    return friends;
  }

  /**
   * return an array of users friends  with the user
   * @param login :String user login
   */
  async getFriendOf(login: string) {
    const user = await this.UserRepository.findOne({
      where: { login: login },
    } as FindManyOptions<User>).catch(() => {
      return { error: 'user not found' };
    });

    if (!(user instanceof User))
        return {error: 'user not found'};
    if (!user) {
      return { error: 'user not found' };
    }
    const relation = await this.FriendsRepository.find({
      relations: { friend_1: true, friend_2: true },
      where: [{ friend_1: { login: login } }, { friend_2: { login: login } }],
    } as FindManyOptions<Friends>).catch(() => {
      return { error: 'friends not found' };
    });

    if (!Array.isArray(relation)) return relation;
    if (relation.length < 1) return { error: 'friends not found' };
    const friends = relation.map((relation) => {
      if (relation.friend_1.id === user.id) {
        const status = this.socketService.getStatusByLogin(
          relation.friend_2.login,
        );

        return { ...relation.friend_2, status: status };
      } else {
        const status = this.socketService.getStatusByLogin(
          relation.friend_1.login,
        );

        return { ...relation.friend_1, status: status };
      }
    });

    return friends;
  }

  /**
   * return an array of users friends  with the user
   * @param login :String user login
   */
  async getConnectedFriendOf(login: string) {
    let connectedFriends = await this.getFriendOf(login);

    if (!Array.isArray(connectedFriends)) return connectedFriends;
    connectedFriends = connectedFriends.filter((friend) => {
      const status: ConnexionStatus = this.socketService.getStatusByLogin(
        friend.login,
      );

      return status === ConnexionStatus.CONNECTED;
    });
    return connectedFriends;
  }

  /**
   * get the friendship between friend 1 and friend 2
   * @param friend_1Login :String friend 1 login
   * @param friend_2Login :String friend 2 login
   */
  async getRelation(friend_1Login: string, friend_2Login: string) {
    const relation1 = await this.FriendsRepository.findOne({
      relations: { friend_1: true, friend_2: true },
      where: [
        {
          friend_1: { login: friend_1Login },
          friend_2: { login: friend_2Login },
        },
      ],
    } as FindManyOptions<Friends>).catch(() => {
      return { error: 'friends not found' };
    });

    if (relation1 instanceof Friends) return relation1;
    if (relation1 !== null) return relation1;

    const relation2 = await this.FriendsRepository.findOne({
      relations: { friend_1: true, friend_2: true },
      where: [
        {
          friend_1: { login: friend_2Login },
          friend_2: { login: friend_1Login },
        },
      ],
    } as FindManyOptions<Friends>).catch(() => {
      return { error: 'friends not found' };
    });

    if (relation2 instanceof Friends) return relation2;
    if (relation2 !== null) return relation2;
    return null;
  }
}