all:
	docker compose up --build

build:
	docker compose build $(ARGS)

up:
	docker compose up 

down:
	docker compose down 
