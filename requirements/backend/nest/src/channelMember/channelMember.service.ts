import { ConflictException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { channelMember } from '../database/entities/channelMember.entity';
import {channelService} from "../channel/channel.service";
import {channel} from "../database/entities/channel.entity";

@Injectable()
export class channelMemberService {
  constructor(
    @InjectRepository(channelMember)
    readonly channelMemberRepository: Repository<channelMember>,
  ) {}

  // METHODS ************************************ //

  /**
   * add a channelMember to the database
   */
  async create(user: channelMember) {
    return await this.channelMemberRepository.save(user).catch((error) => {
      throw error;
    });
  }

  /**
   * remove a channelMember with its id
   */
  async delete(userId: number) {
    return await this.channelMemberRepository.delete(userId).catch((error) => {
      throw error;
    });
  }

  /**
   * get channels where the user is
   */
  async getChannels(userLogin: string) {
    const channels = await this.channelMemberRepository
      .find({
        relations: { channel: true, user: true },
        where: { user: { login: userLogin } },
      })
      .catch((error) => {
        throw error;
      });

    if (!channels || channels.length < 1) {
      throw new ConflictException('the users is not in a chatList.vue');
    }

    const filter = [] as channelMember[];
    channels.map((info) => {
      if (!info.blocked && info.register) {
        filter.push(info);
        return info;
      }
    });

    return filter;
  }

  /**
   * get channels where the user is invited
   * @param userLogin
   */
  async getChannelsInvited(userLogin: string) {
    const channels = await this.channelMemberRepository
      .find({
        relations: { channel: true, user: true },
        where: { user: { login: userLogin } },
      })
      .catch((error) => {
        throw error;
      });

    if (!channels || channels.length < 1) {
      throw new ConflictException('the users is not in a chatList.vue');
    }

    const filter = [] as channel [];
    channels.map((info) => {
      if (!info.blocked && !info.register) {
        filter.push(info.channel);
        return info.channel;
      }
    });

    return filter;
  }

  /**
   * get users for a specific channel
   */
  async getUsers(channelName: string) {
    const users = await this.channelMemberRepository
      .find({
        relations: { channel: true, user: true },
        where: { channel: { name: channelName } },
      })
      .catch(() => {
        return { error: 'users not found' };
      });

    if (!Array.isArray(users)) return users;

    if (!users || users.length < 1) {
      throw new ConflictException('empty chatList.vue');
    }

    const filter: channelMember[] = [] as channelMember [];
    users.map((info) => {
      if (!info.blocked && info.register) {
        filter.push(info);
        return info;
      }
    });

    return filter.map((users) => {
      return users.user;
    });
  }

  /**
   * get the channelMember info for a specific channel
   */
  async getUserInfo(userLogin: string, channelName: string) {
    return await this.channelMemberRepository
      .findOne({
        relations: { channel: true, user: true },
        where: {
          channel: { name: channelName },
          user: { login: userLogin },
        },
      })
      .catch(() => {
        return { error: 'user not found' };
      });
  }
}
