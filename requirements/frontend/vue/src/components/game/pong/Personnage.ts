import { Rect, Vector2i } from "./game.type";

export class Personnage{
	r: Rect;
	move_down: boolean;
	move_up: boolean;
	color: string;

	constructor(x: number, y: number, color: string) {
		this.r = {x: x, y: y, w: 5, h: 30}
		this.move_down = false;
		this.move_up = false;
		this.color = color;
	}

	draw(ctx: CanvasRenderingContext2D , ratio: number, shift: Vector2i){
		this.onMove();
		ctx.fillStyle = "white";
		setShadow(ctx, this.color, 20);
		ctx.fillRect((this.r.x + shift.x) * ratio, (this.r.y + shift.y) * ratio, this.r.w * ratio, this.r.h * ratio);
		endShadow(ctx);
	}

	onMove() {
		const speed = 6;
		if (this.move_down == true)
			if (this.r.y + speed < 242 - this.r.h)
				this.r.y += speed;
		if (this.move_up == true)
			if (this.r.y + speed > 25)
				this.r.y -= speed;
	}
	down(val: boolean) {
		this.move_down = val;
	}
	up(val: boolean) {
		this.move_up = val;
	}
}

function setShadow(ctx: CanvasRenderingContext2D, color: string, force: number) {
	ctx.shadowColor = color;
	ctx.shadowBlur = force;
	ctx.beginPath();
	ctx.fill();
}

function endShadow(ctx: CanvasRenderingContext2D) {
	ctx.shadowColor = 'transparent';
	ctx.shadowBlur = 0;
}
