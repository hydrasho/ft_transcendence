import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ApiService } from '../../api/api.service';
import { channelMemberService } from '../../channelMember/channelMember.service';
import { channelMember } from '../../database/entities/channelMember.entity';

@Injectable()
export class channelMemberGuard implements CanActivate {
  constructor(
    private apiService: ApiService,
    public channelMemberService: channelMemberService,
  ) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const body = request.body;
    const path = request.url;
    const cookie: string = request.cookies['jwt'];
    if (!cookie) return false;

    const login = await this.apiService.getCookieLogin(request);
    let channelName: string = body.channel;
    if (!channelName) channelName = path.replace('/channelMsg/discussion/', '');

    if (!login || !channelName) return false;

    const memberData = await this.channelMemberService.getUserInfo(
      login,
      channelName,
    );
    if (!(memberData instanceof channelMember)) return false;
    if (!memberData) return false;
    if (memberData.blocked) return false;
    if (!memberData.register) return false;
    return true;
  }
}
