import {
  Body,
  ConflictException,
  Controller,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { ApiService } from '../api/api.service';
import { Request } from 'express';
import { channelMsgService } from './channelMsg.service';
import { channelMsgDTO } from '../DTO/channelMsg.DTO';
import { channelService } from '../channel/channel.service';
import { CookieAuthGuard } from '../guardians/cookies/cookies.guard';
import { channelMemberGuard } from '../guardians/channels/channelMember.guard';
import { User } from '../database/entities/user.entity';
import { BlockUserService } from '../blockUsers/blockUser.service';
import { channel } from '../database/entities/channel.entity';
import { channelMemberService } from '../channelMember/channelMember.service';
import { channelMember } from '../database/entities/channelMember.entity';

@Controller('channelMsg')
export class channelMsgController {
  constructor(
    private readonly blockUserService: BlockUserService,
    private readonly memberChannelService: channelMemberService,
    private readonly channelMsgService: channelMsgService,
    private readonly channelService: channelService,
    private readonly usersService: UsersService,
    private readonly apiService: ApiService,
  ) {}

  /**
   * add a new message to a channel
   */
  @Post()
  @UseGuards(CookieAuthGuard)
  @UseGuards(channelMemberGuard)
  async add(@Body() messageInfo: channelMsgDTO) {
    const messageData = this.channelMsgService.channelMsgRepository.create();
    const channel = await this.channelService.channelRepository.findOne({
      where: { name: messageInfo.channel },
    });
    const sender = await this.usersService.getSingleUser_bylogin(
      messageInfo.sender,
    );
    const member = await this.memberChannelService.getUserInfo(
      messageInfo.sender,
      messageInfo.channel,
    );

    if (!member || !(member instanceof channelMember))
      return { error: 'user not found' };
    if (!sender || !(sender instanceof User))
      return { error: 'user not found' };
    if (!channel) return { error: 'channel not found' };
    if (messageInfo.message.length < 1 ||messageInfo.message.length > 1000)
      throw new ConflictException('message error');

    if (member.end_mute && member.end_mute > new Date())
      return { error: 'user muted' };
    else if (member.end_mute) {
      member.end_mute = null;
      await this.memberChannelService.channelMemberRepository.save(member);
    }

    messageData.sender = sender;
    messageData.channel = channel;
    messageData.message = messageInfo.message;

    return await this.channelMsgService.create(messageData);
  }

  /**
   * get all messages from a channel with its name
   */
  @Get('discussion/:channelName')
  @UseGuards(channelMemberGuard)
  @UseGuards(CookieAuthGuard)
  async getChannelMsg(
    @Req() request: Request,
    @Param('channelName') channelName: string,
  ) {
    const user = await this.apiService.getCookieLogin(request);
    if (!channelName) throw new ConflictException('channel name empty');
    const Channel = await this.channelService.getChannelByName(channelName);

    if (!(Channel instanceof channel)) return Channel;

    if (!user || !Channel) return { error: 'user or channel not found' };

    const discussion = await this.channelMsgService.getChannelMsg(Channel.name);

    if (!Array.isArray(discussion)) return discussion;
    const discussionFilterPromises = discussion.map(async (msg) => {
      const block = await this.blockUserService.getRelation(
        user,
        msg.sender.login,
      );
      if (msg.sender.login !== user && block)
        msg.message = '--- User blocked ---';
      return msg;
    });

    return await Promise.all(discussionFilterPromises);
  }
}
