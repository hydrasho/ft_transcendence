import { Module } from '@nestjs/common';
import { ApiController } from './api.controller';
import { ApiService } from './api.service';
import { UsersModule } from '../users/users.module';
import { AppModule } from '../app.module';

@Module({
  imports: [UsersModule, AppModule], // New modules must be imported here
  controllers: [ApiController],
  providers: [ApiService],
})
export class ApiModule {}
