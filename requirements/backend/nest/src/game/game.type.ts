export interface Rect {
  x: number;
  y: number;
  w: number;
  h: number;
}

export interface Velocity {
  x: number;
  y: number;
}

export interface Vector2i {
  x: number;
  y: number;
}
export interface Segment {
  start: Vector2i;
  end: Vector2i;
}

export class Collision {
  static check(r1: Rect, r2: Rect): boolean {
    if (
      r1.x < r2.x + r2.w &&
      r1.x + r1.w > r2.x &&
      r1.y < r2.y + r2.h &&
      r1.y + r1.h > r2.y
    )
      return true;
    return false;
  }

  static goThrough(
    ballBef: Rect,
    ballAfter: Rect,
    ballVelocity: Vector2i,
    obj: Rect,
  ) {
    const ballTMP: Rect = {
      x: ballBef.x,
      y: ballBef.y,
      h: ballBef.h,
      w: ballBef.w,
    };

    if (Collision.check(ballAfter, obj)) return true;
    while (Collision.check(ballTMP, ballAfter) === false) {
      if (Collision.check(ballTMP, obj)) return true;
      ballTMP.x += ballVelocity.x * 5;
      ballTMP.y += ballVelocity.y * 5;
    }
    return false;
  }
}
