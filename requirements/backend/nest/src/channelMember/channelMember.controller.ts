import { Controller, Get, Param, Req, UseGuards } from '@nestjs/common';
import { channelMemberService } from './channelMember.service';
import { CookieAuthGuard } from '../guardians/cookies/cookies.guard';
import { ApiService } from '../api/api.service';
import { Request } from 'express';

@Controller('channelMember')
export class channelMemberController {
  constructor(
    private readonly channelMemberService: channelMemberService,
    private readonly apiService: ApiService,
  ) {}

  /**
   * Get the list of channels where the user is
   */
  @Get('Channels/')
  @UseGuards(CookieAuthGuard)
  async getChannels(@Req() request: Request) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    const channels = await this.channelMemberService.getChannels(userLogin);
    for (const info of channels) {
      info.channel.pass = null;
    }
    return channels;
  }

  /**
   * Get the list of users from a chatList.vue
   */
  @Get('Users/:channelName')
  @UseGuards(CookieAuthGuard)
  async getUsers(@Param('channelName') channelName: string) {
    return await this.channelMemberService.getUsers(channelName);
  }

  /**
   * Get the channelMember info of the user for a specific channel
   */
  @Get('UserInfo/:channelName')
  @UseGuards(CookieAuthGuard)
  async getUserInfo(
    @Req() request: Request,
    @Param('channelName') channelName: string,
  ) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    return await this.channelMemberService.getUserInfo(userLogin, channelName);
  }

  /**
   * Get the list of channels where the user is invited
   */
  @Get('channelsInvited/')
  @UseGuards(CookieAuthGuard)
  async getChannelsInvited(@Req() request: Request) {
    const userLogin = await this.apiService.getCookieLogin(request);
    if (!userLogin) return { error: 'user not found' };
    const channels = await this.channelMemberService.getChannelsInvited(userLogin);
    for (const channel of channels)
      channel.pass = null;
    return channels;
  }
}
