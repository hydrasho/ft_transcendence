import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Query,
  Req,
  Res,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { ApiService } from './api.service';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { Response, Request } from 'express';
import { User } from '../database/entities/user.entity';
import { isJWT } from 'class-validator';
import { CookieAuthGuard } from '../guardians/cookies/cookies.guard';

/** Controller called when asking API on: `IP:back_port` **/
@Controller('api')
export class ApiController {
  constructor(
    private readonly apiService: ApiService,
    private readonly usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  /**
   * create and return the cookie authenticator
   * @param login :String user login to use
   * @param response where to cookie is stored
   */
  @Post('token')
  async createCookie(
    @Body('login') login: string,
    @Res({ passthrough: true }) response: Response,
  ) {
    const user = await this.usersService
      .getSingleUser_bylogin(login)
      .catch(() => {
        return { error: 'invalid credentials' };
      });
    if (!(user instanceof User) && user.error) {
      return user;
    }
    if (!user) throw new BadRequestException('invalid credentials');

    if (user instanceof User) {
      const token = await this.jwtService.signAsync({ login: user.login });
      response.cookie('jwt', token);
      return {
        message: 'success',
      };
    } else {
      return { error: 'failed' };
    }
  }

  @Post('getuser')
  async getUser_withIntra(@Body('token') token: string) {
    if (!token || token.length !== 64) return { error: 'invalid token' };
    try {
      const request = await fetch('https://api.intra.42.fr/v2/me', {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (!request.ok) {
        return { error: 'invalid token' };
      }
      const content = await request.json();
      if (!content || !content.login) {
        return { error: 'invalid token' };
      }
      return { login: content.login };
    } catch (error) {
      return { error: 'invalid token' };
    }
  }

	/**
	 * exchange code to token
	 * @param code code used to exchange
	 * @param response
	 */
	@Get('gettoken')
	async getToken(@Query('code') code: string,
				   @Res({passthrough: true}) response: Response) {
		const url = new URL('http://' + process.env.IP_HOST);
		url.port = process.env.FRONT_PORT;
		url.pathname = '404';

		if (!code || code.length != 64) {
			await response.status(404).redirect(url.href);
			return ;
		}
		try {
			const request = await fetch('https://api.intra.42.fr/oauth/token', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					grant_type: "authorization_code",
					client_id:
						process.env.CLIENT_ID,
					client_secret:
						process.env.SECRET_ID,
					code: code,
					redirect_uri: 'http://' + process.env.IP_HOST + ':' + process.env.BACK_PORT + '/api/gettoken/',
				}),
			});

			if (!request.ok) {
				await response.status(404).redirect(url.href);
				return ;
			}
			const content = await request.json();
			if (!content || !content.access_token) {
				await response.status(404).redirect(url.href);
				return ;
			}
			await response.cookie('_uid', content.access_token);
			const intraUrl = new URL('http://' + process.env.IP_HOST);
			intraUrl.port = process.env.FRONT_PORT;
			intraUrl.pathname = 'intra-connect';
			intraUrl.searchParams.set('code', content.access_token);
			await response.status(302).redirect(intraUrl.href);
			return ;
		} catch (error) {
			console.error(error);
			await response.status(404).redirect(url.href);
			return ;
		}
	}

  /**
   * check if the cookie is correct
   * @param request where the cookie is stored
   */
  @Get('user')
  @UseGuards(CookieAuthGuard)
  async user(@Req() request: Request) {
    try {
      const cookie = request.cookies['jwt'];

      if (!isJWT(cookie)) return false;

      const data = await this.jwtService.verifyAsync(cookie);

      if (!data) throw new UnauthorizedException();

      const user = await this.usersService.getSingleUser_bylogin(data['login']);

      return user;
    } catch (error) {
      throw new UnauthorizedException(error.message);
    }
  }

  /**
   * delete the cookie
   * @param response where the cookie is stored
   */
  @Post('logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');

    return {
      message: 'success',
    };
  }

  /**
   * get the login with the cookie
   * @param request
   */
  @Get('getLogin')
  @UseGuards(CookieAuthGuard)
  async getLogin(@Req() request: Request) {
    const login = await this.apiService.getCookieLogin(request);
    return { login: login };
  }
}
