import { Module } from '@nestjs/common';
import { AppModule } from '../app.module';
import { channelMsgService } from './channelMsg.service';
import { channelMsgController } from './channelMsg.controller';

@Module({
  imports: [AppModule], // New modules must be imported here
  controllers: [channelMsgController],
  providers: [channelMsgService],
})
export class channelMsgModule {}
