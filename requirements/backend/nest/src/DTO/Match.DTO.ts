import {
  IsBoolean,
  IsDate,
  IsNotEmpty,
  IsNumber,
  IsString,
} from 'class-validator';

export class MatchDTO {
  @IsDate()
  @IsNotEmpty()
  timestamp: Date;

  @IsString()
  @IsNotEmpty()
  id: string;

  @IsString()
  @IsNotEmpty()
  left_player: string;

  @IsString()
  @IsNotEmpty()
  right_player: string;

  @IsNumber()
  @IsNotEmpty()
  left_player_score: number;

  @IsNumber()
  @IsNotEmpty()
  right_player_score: number;

  @IsBoolean()
  @IsNotEmpty()
  is_left_winner: boolean;
}
