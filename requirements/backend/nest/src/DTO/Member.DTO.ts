import { MemberType } from '../database/enums/MemberType.enum';
import { User } from '../database/entities/user.entity';
import { channel } from '../database/entities/channel.entity';
import {
  IsBoolean,
  IsDate,
  IsEmpty,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class MemberDTO {
  @IsOptional()
  @IsNumber()
  id: number;

  @IsEmpty()
  channel: channel;

  @IsNumber()
  @IsNotEmpty()
  user: User;

  @IsString()
  @IsNotEmpty()
  type: MemberType;

  @IsBoolean()
  @IsNotEmpty()
  register: boolean;

  @IsBoolean()
  @IsOptional()
  muted: boolean;

  @IsBoolean()
  @IsOptional()
  blocked: boolean;

  @IsDate()
  @IsOptional()
  end_mute: Date;
}
