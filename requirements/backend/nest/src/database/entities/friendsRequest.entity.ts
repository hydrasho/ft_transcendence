import {
  BeforeInsert,
  BeforeUpdate,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity('FriendsRequest')
export class FriendsRequest {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user1) => user1.id, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn()
  sender: User;

  @ManyToOne(() => User, (user2) => user2.id, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn()
  receiver: User;

  @BeforeInsert()
  @BeforeUpdate()
  checkColumnEquqlity() {
    if (this.sender === this.receiver)
      throw new Error('friend_1 and friend_2 must be different');
  }
}
