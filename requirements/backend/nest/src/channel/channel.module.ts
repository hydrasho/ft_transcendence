import { Module } from '@nestjs/common';
import { AppModule } from '../app.module';
import { channelService } from './channel.service';
import { channelController } from './channel.controller';

@Module({
  imports: [AppModule], // New modules must be imported here
  controllers: [channelController],
  providers: [channelService],
})
export class channelModule {}
