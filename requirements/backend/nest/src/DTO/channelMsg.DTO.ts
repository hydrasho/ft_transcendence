import {IsNotEmpty, IsString, MaxLength, MinLength} from 'class-validator';

export class channelMsgDTO {
  @IsString()
  @IsNotEmpty()
  channel: string;

  @IsString()
  @IsNotEmpty()
  sender: string;

  @IsString()
  @MinLength(1)
  @MaxLength(1000)
  @IsNotEmpty()
  message: string;
}
