import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ApiController } from './api/api.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './database/entities/user.entity';
import { Friends } from './database/entities/friends.entity';
import { Tfa } from './database/entities/tfa.entity';
import { UsersController } from './users/users.controller';
import { FriendController } from './friends/friends.controller';
import { TfaController } from './2fa/tfa.controller';
import { UsersService } from './users/users.service';
import { FriendsService } from './friends/friends.service';
import { TfaService } from './2fa/tfa.service';
import { ApiService } from './api/api.service';
import { JwtModule } from '@nestjs/jwt';
import { FriendsRequest } from './database/entities/friendsRequest.entity';
import { Friends_requestController } from './friends_request/friends_request.controller';
import { Friends_requestService } from './friends_request/friends_request.service';
import { channel } from './database/entities/channel.entity';
import { channelController } from './channel/channel.controller';
import { channelService } from './channel/channel.service';
import { channelMember } from './database/entities/channelMember.entity';
import { channelMemberController } from './channelMember/channelMember.controller';
import { channelMemberService } from './channelMember/channelMember.service';
import { privMsg } from './database/entities/privMsg.entity';
import { privMsgController } from './privMsg/privMsg.controller';
import { privMsgService } from './privMsg/privMsg.service';
import { channelMsg } from './database/entities/channelMsg.entity';
import { channelMsgController } from './channelMsg/channelMsg.controller';
import { channelMsgService } from './channelMsg/channelMsg.service';
import { SocketsGateway } from './sockets/sockets.gateway';
import { SocketsService } from './sockets/sockets.service';
import { GameGateway } from './game/game.gateway';
import { GameService } from './game/game.service';
import { GameLobbyService } from './game_lobby/game_lobby.service';
import { GameLobbyGateway } from './game_lobby/game_lobby.gateway';
import { Match } from './database/entities/match.entity';
import { GameController } from './game/game.controller';
import { blockUser } from './database/entities/blockUser.entity';
import { BlockUserController } from './blockUsers/blockUser.controller';
import { BlockUserService } from './blockUsers/blockUser.service';

@Module({
    imports: [AppModule,
        TypeOrmModule.forRoot({
            type: 'postgres',
            host: 'postgres',
            port: 5432,
            username: process.env.POSTGRES_USER,
            password: process.env.POSTGRES_PASSWORD,
            database: process.env.POSTGRES_DB,
            entities: [
                Tfa,
                User,
                Match,
                Friends,
                channel,
                privMsg,
                blockUser,
                channelMsg,
                channelMember,
                FriendsRequest,
            ],
            synchronize: true,
        }),
        TypeOrmModule.forFeature([
            Tfa,
            User,
            Match,
            Friends,
            channel,
            privMsg,
            blockUser,
            channelMsg,
            channelMember,
            FriendsRequest,
        ]),
        JwtModule.register({
            secret: process.env.JWT_SECRET,
            signOptions: {expiresIn: '1d'}
        }),
    ],
    controllers: [
        TfaController,
        AppController,
        ApiController,
        UsersController,
        FriendController,
        channelController,
        privMsgController,
        BlockUserController,
        channelMsgController,
        channelMemberController,
        GameController, Friends_requestController,
    ],
    providers: [
        AppService,
        TfaService,
        ApiService,
        GameGateway,
        GameService,
        UsersService,
        FriendsService,
        channelService,
        privMsgService,
        SocketsGateway,
        SocketsService,
        GameLobbyService,
        GameLobbyGateway,
        BlockUserService,
        channelMsgService,
        channelMemberService,
        Friends_requestService,
    ],
})
export class AppModule {}
