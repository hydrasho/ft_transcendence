
/** Define type User **/
interface User {
    id: number;
    username: string;
    login: string;
    using_2fa: boolean;
    avatar_file: string;
}

export type Friend = {
    id: number,
    friend_1: User,
    friend_2: User
}
