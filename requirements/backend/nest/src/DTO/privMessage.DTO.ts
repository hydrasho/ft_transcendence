import {IsNotEmpty, IsString, MaxLength, MinLength} from 'class-validator';

export class privMessageDTO {
  @IsString()
  @IsNotEmpty()
  senderLogin: string;

  @IsString()
  @IsNotEmpty()
  receiverLogin: string;

  @IsString()
  @MinLength(1)
  @MaxLength(1000)
  @IsNotEmpty()
  message: string;
}
