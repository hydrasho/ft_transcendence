export enum MemberType {
  OWNER = 'owner',
  ADMIN = 'admin',
  NORMAL = 'normal',
}
