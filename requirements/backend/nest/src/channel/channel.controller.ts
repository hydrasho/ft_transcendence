import {
  Body,
  ConflictException,
  Controller,
  ForbiddenException,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import {channelService} from './channel.service';
import {NewMemberDTO} from '../DTO/NewMember.DTO';
import {UsersService} from '../users/users.service';
import {NewChannelDTO} from '../DTO/NewChannel.DTO';
import {User} from '../database/entities/user.entity';
import {channel} from '../database/entities/channel.entity';
import {MemberType} from '../database/enums/MemberType.enum';
import {ChannelType} from '../database/enums/ChannelType.enum';
import {CookieAuthGuard} from '../guardians/cookies/cookies.guard';
import {channelMember} from '../database/entities/channelMember.entity';
import {channelMemberService} from '../channelMember/channelMember.service';
import {channelsRightGuard} from '../guardians/channels/channelsRight.guard';
import {OwnerRightGuard} from "../guardians/channels/OwnerAccess.guard";

@Controller('channel')
export class channelController {
  constructor(
    private readonly channelService: channelService,
    private readonly channelMemberService: channelMemberService,
    private readonly userService: UsersService,
  ) {}

  /**
   * Create a new channel and set the user as owner
   */
  @Post()
  @UseGuards(CookieAuthGuard)
  async add(@Body() request: NewChannelDTO) {
    const search = await this.channelService.getChannelByName(
      request.channelInfo.name,
    );
    if (search) {
      throw new ConflictException('chatList.vue already exist');
    }
    const user = await this.userService.getSingleUser_bylogin(
      request.userLogin,
    );
    if (!user || !(user instanceof User)) {
      return { error: 'owner not found' };
    }

    if (request.channelInfo.pass === '') request.channelInfo.pass = null;

    const passRegex =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&+=])(?!.*\s).{8,20}$/;
    const nameRegex = /^[a-z]{4,10}$/;
    if (request.channelInfo.type === ChannelType.PUBLIC) {
      request.channelInfo.pass = null;
    } else if (!passRegex.test(request.channelInfo.pass)) {
      throw new ConflictException('invalid credential');
    }
    if (!nameRegex.test(request.channelInfo.name)) {
      throw new ConflictException('invalid credential');
    }

    if (request.channelInfo.type !== ChannelType.PUBLIC) {
      const salt = await bcrypt.genSalt();
      request.channelInfo.pass = await bcrypt.hash(
        request.channelInfo.pass,
        salt,
      );
    }

    if (
      request.channelInfo.type !== ChannelType.PUBLIC &&
      !request.channelInfo.pass
    ) {
      throw new ConflictException('you have to set a password');
    }
    const Channel = await this.channelService.create(request.channelInfo);

    const newMember: channelMember = {
      id: 0,
      channel: Channel,
      user: user,
      muted: false,
      blocked: false,
      register: true,
      type: MemberType.OWNER,
      end_mute: null,
    };
    await this.channelMemberService.create(newMember);

    const { pass, ...retValue } = Channel;
    return retValue;
  }

  /**
   * search a channel with its name and return it if the channel is found
   */
  @Get(':name')
  @UseGuards(CookieAuthGuard)
  async getByName(@Param('name') channelName: string) {
    const search = await this.channelService.getChannelByName(channelName);

    if (!(search instanceof channel)) return search;
    const { pass, ...retValue } = search;
    return retValue;
  }

  /**
   * Get the list of the channels with a specific type
   * @param channelType channel type
   */
  @Get('/type/:type')
  @UseGuards(CookieAuthGuard)
  async getByType(@Param('type') channelType: ChannelType) {
    const search = await this.channelService.getChannelsByType(channelType);

    if (!Array.isArray(search)) return search;
    else if (Array.isArray(search)) {
      const protectedChannel = search?.map((row) => {
        const { pass, ...retValue } = row;
        return retValue;
      });

      if (protectedChannel.length < 1) return { error: 'channels not found' };

      return protectedChannel;
    }
  }

  /**
   * edit the password of a specific channel
   */
  @Patch('password')
  @UseGuards(CookieAuthGuard)
  @UseGuards(OwnerRightGuard)
  async editPass(
    @Body('channelName') name: string,
    @Body('password') password: string,
  ) {
    const Channel = await this.channelService.getChannelByName(name);

    if (!(Channel instanceof channel)) return Channel;
    if (!Channel) throw new ConflictException('The chatList.vue doesnt exist');

    if (password === '')
      password = null;
    const passRegex= /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&+=])(?!.*\s).{8,20}$/;
    if (password && (password.length < 8 || password.length > 20))
      throw new ConflictException('invalid credential');
    if (password && !passRegex.test(password)) {
      throw new ConflictException('invalid credential');
    }

   if (password) {
     const salt = await bcrypt.genSalt();
     password = await bcrypt.hash(password, salt);
   }

    const channelEdit = await this.channelService.changePass(Channel, password);

    const { pass, ...retValue } = channelEdit;
    return retValue;
  }

  /**
   * add a new member to a channel
   * @param info contains the channel name, the login and the pass
   */
  @Patch('addMember')
  @UseGuards(CookieAuthGuard)
  async addMember(@Body() info: NewMemberDTO) {
    const Channel = await this.channelService.getChannelByName(
      info.channelName,
    );
    if (!Channel || !(Channel instanceof channel))
      return { error: 'channel not found' };

    const user = await this.userService.getSingleUser_bylogin(info.login);
    if (!user || !(user instanceof User)) return { error: 'user not found' };

    const userChannel = await this.channelMemberService.getUserInfo(
      info.login,
      info.channelName,
    );

    if (userChannel instanceof channelMember) {
      if (userChannel && userChannel.register)
        throw new ConflictException('is already member');
      else if (userChannel && userChannel.blocked)
        throw new ForbiddenException ('you have been blocked');
    } else {
      if (!userChannel && Channel.type === 'private')
        return { error: 'channel private' };
    }
    if (Channel.pass) {
      if (!info.pass || !(await bcrypt.compare(info.pass, Channel.pass)))
        throw new ConflictException('add member error');
    }
    if (info.type !== MemberType.NORMAL) {
      throw new ConflictException('you cant give admin rights');
    }
    const newMember: channelMember = {
      id: 0,
      channel: Channel,
      user: user,
      muted: false,
      blocked: false,
      register: true,
      type: info.type,
      end_mute: null,
    };
    if (userChannel instanceof channelMember) {
      userChannel.register = true;
      this.channelService.newActionEvent(Channel.name, 'new', info.login);
      return await this.channelMemberService.channelMemberRepository.save(
        userChannel,
      );
    }
    const addMember = await this.channelMemberService.create(newMember);
    this.channelService.newActionEvent(Channel.name, 'new', info.login);
    addMember.channel.pass = null;
    return addMember;
  }

  /**
   * remove a user from a channel
   * @param info contains the channel name, the login and the pass
   */
  @Patch('deleteMember')
  @UseGuards(CookieAuthGuard)
  @UseGuards(channelsRightGuard)
  async deleteMember(@Body() info: NewMemberDTO) {
    const Channel = await this.channelService.getChannelByName(
      info.channelName,
    );
    if (!Channel || !(Channel instanceof channel))
      return { error: 'channel not found' };

    const user = await this.userService.getSingleUser_bylogin(info.login);
    if (!user) return { error: 'user not found' };

    const userChannel = await this.channelMemberService.getUserInfo(
      info.login,
      Channel.name,
    );

    if (!(userChannel instanceof channelMember)) return userChannel;
    if (!userChannel) throw new ConflictException('is already member');

    const response = await this.channelMemberService.delete(userChannel.id);

    const Users = await this.channelMemberService
      .getUsers(Channel.name)
      .catch(async (error) => {
        if (error.message === 'empty chatList.vue') {
          this.channelService.newActionEvent(Channel.name, 'leave', info.login);
          await this.channelService.delete(Channel.id);
        } else throw error;
      });
    if (!Array.isArray(Users)) return Users;
    if (!Users || Users.length < 1) {
      this.channelService.newActionEvent(Channel.name, 'leave', info.login);
      await this.channelService.delete(Channel.id);
    }
    this.channelService.newActionEvent(Channel.name, 'leave', info.login);
    return response;
  }

  /**
   * invite a user to a private channel
   * @param info contains the channel name, the login and the pass
   */
  @Patch('invite')
  @UseGuards(CookieAuthGuard)
  @UseGuards(channelsRightGuard)
  async inviteMember(@Body() info: NewMemberDTO) {
    const Channel = await this.channelService.getChannelByName(
      info.channelName,
    );
    if (!Channel || !(Channel instanceof channel))
      return { error: 'channel not found' };
    if (Channel.type !== 'private')
      throw new ConflictException('the channel is not private');

    const user = await this.userService.getSingleUser_bylogin(info.login);
    if (!user || !(user instanceof User)) return { error: 'user not found' };

    const userChannel = await this.channelMemberService.getUserInfo(
      info.login,
      info.channelName,
    );
    if (userChannel instanceof channelMember) {
      throw new ConflictException('dont need to be invited');
    }
    if (info.type !== MemberType.NORMAL) {
      throw new ConflictException('you cant give admin rights');
    }

    const newMember: channelMember = {
      id: 0,
      channel: Channel,
      user: user,
      muted: false,
      blocked: false,
      register: false,
      type: info.type,
      end_mute: null,
    };
    const addMember = await this.channelMemberService.create(newMember);
    this.channelService.newActionEvent(Channel.name, 'invite', info.login);
    addMember.channel.pass = null;
    return addMember;
  }

  /**
   * promote a user to the admin right
   * @param info contains the channel name, the login and the pass
   */
  @Patch('promote')
  @UseGuards(CookieAuthGuard)
  @UseGuards(channelsRightGuard)
  @UseGuards(OwnerRightGuard)
  async promoteMember(@Body() info: NewMemberDTO) {
    const Channel = await this.channelService.getChannelByName(
      info.channelName,
    );
    if (!Channel || !(Channel instanceof channel))
      return { error: 'channel not found' };

    const user = await this.userService.getSingleUser_bylogin(info.login);
    if (!user || !(user instanceof User)) return { error: 'user not found' };

    const userChannel = await this.channelMemberService.getUserInfo(
      info.login,
      info.channelName,
    );
    if (!(userChannel instanceof channelMember)) {
      return userChannel;
    }
    if (userChannel.type !== MemberType.NORMAL)
      throw new ConflictException('user already admin');

    userChannel.type = MemberType.ADMIN;

    const addMember =
      await this.channelMemberService.channelMemberRepository.save(userChannel);
    this.channelService.newActionEvent(Channel.name, 'promote', info.login);
    addMember.channel.pass = null;
    return addMember;
  }

  /**
   * demote a user to the admin right
   * @param info contains the channel name, the login and the pass
   */
  @Patch('demote')
  @UseGuards(CookieAuthGuard)
  @UseGuards(channelsRightGuard)
  @UseGuards(OwnerRightGuard)
  async demoteMember(@Body() info: NewMemberDTO) {
    const Channel = await this.channelService.getChannelByName(
      info.channelName,
    );
    if (!Channel || !(Channel instanceof channel))
      return { error: 'channel not found' };

    const user = await this.userService.getSingleUser_bylogin(info.login);
    if (!user || !(user instanceof User)) return { error: 'user not found' };

    const userChannel = await this.channelMemberService.getUserInfo(
      info.login,
      info.channelName,
    );
    if (!(userChannel instanceof channelMember)) {
      return userChannel;
    }
    if (userChannel.type === MemberType.NORMAL)
      throw new ConflictException('user already normal');
    if (userChannel.type === MemberType.OWNER)
      throw new ConflictException('user is the owner');

    userChannel.type = MemberType.NORMAL;

    const addMember =
      await this.channelMemberService.channelMemberRepository.save(userChannel);
    this.channelService.newActionEvent(Channel.name, 'demote', info.login);
    addMember.channel.pass = null;
    return addMember;
  }

  /**
   * ban a user to the channel
   * @param info contains the channel, the login and the pass
   */
  @Patch('ban')
  @UseGuards(CookieAuthGuard)
  @UseGuards(channelsRightGuard)
  async banMember(@Body() info: NewMemberDTO) {
    const Channel = await this.channelService.getChannelByName(
      info.channelName,
    );
    if (!Channel || !(Channel instanceof channel))
      return { error: 'channel not found' };

    const user = await this.userService.getSingleUser_bylogin(info.login);
    if (!user || !(user instanceof User)) return { error: 'user not found' };

    const userChannel = await this.channelMemberService.getUserInfo(
      info.login,
      info.channelName,
    );
    if (!(userChannel instanceof channelMember)) {
      return userChannel;
    }

    if (userChannel.type === MemberType.OWNER)
      throw new ConflictException('user is the owner');

    userChannel.type = MemberType.NORMAL;
    userChannel.blocked = true;
    userChannel.register = false;

    const addMember =
      await this.channelMemberService.channelMemberRepository.save(userChannel);
    this.channelService.newActionEvent(Channel.name, 'ban', info.login);
    addMember.channel.pass = null;
    return addMember;
  }

  /**
   * remove a user from a channel
   * @param info contains the channel, the login and the pass
   */
  @Patch('kick')
  @UseGuards(CookieAuthGuard)
  @UseGuards(channelsRightGuard)
  async kickMember(@Body() info: NewMemberDTO) {
    const Channel = await this.channelService.getChannelByName(
      info.channelName,
    );
    if (!Channel || !(Channel instanceof channel))
      return { error: 'channel not found' };

    const user = await this.userService.getSingleUser_bylogin(info.login);
    if (!user) return { error: 'user not found' };

    const userChannel = await this.channelMemberService.getUserInfo(
      info.login,
      Channel.name,
    );

    if (!(userChannel instanceof channelMember)) return userChannel;
    if (!userChannel) throw new ConflictException('is not member');
    if (userChannel.type === MemberType.OWNER) return {error: 'is owner'};

    const response = await this.channelMemberService.delete(userChannel.id);

    const Users = await this.channelMemberService
      .getUsers(Channel.name)
      .catch(async (error) => {
        if (error.message === 'empty chatList.vue')
          await this.channelService.delete(Channel.id);
        else throw error;
      });
    if (!Array.isArray(Users)) return Users;
    if (!Users || Users.length < 1) {
      await this.channelService.delete(Channel.id);
    }
    this.channelService.newActionEvent(Channel.name, 'kick', info.login);
    return response;
  }

  /**
   * mute a member during a limited time for a specific channel
   * @param info contains the channel name, the login and the pass
   */
  @Patch('mute')
  @UseGuards(CookieAuthGuard)
  @UseGuards(channelsRightGuard)
  async muteMember(@Body() info: NewMemberDTO) {
    const Channel = await this.channelService.getChannelByName(
      info.channelName,
    );
    if (!Channel || !(Channel instanceof channel))
      return { error: 'channel not found' };

    const user = await this.userService.getSingleUser_bylogin(info.login);
    if (!user || !(user instanceof User)) return { error: 'user not found' };

    const userChannel = await this.channelMemberService.getUserInfo(
      info.login,
      info.channelName,
    );
    if (!(userChannel instanceof channelMember)) {
      return userChannel;
    }

    if (userChannel.type === MemberType.OWNER) return {error: 'is owner'};

    if (!info.mute) return new ConflictException('date not set');

    if (info.mute < 1 || info.mute > 42)
      return  new ConflictException('error date');
    const currentDate = new Date();
    userChannel.end_mute = new Date(currentDate.getTime() + info.mute * 60000);

    const addMember =
      await this.channelMemberService.channelMemberRepository.save(userChannel);
    this.channelService.newActionEvent(Channel.name, 'mute', info.login);
    addMember.channel.pass = null;
    return addMember;
  }
}
