import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '@/views/Home.vue'
import NotFound from '@/components/NotFound.vue'
import NotAuthorized from '@/components/NotAuthorized.vue'
import HomePage from '@/components/HomePage.vue'
import PongGame from '@/components/game/PongGame.vue'
import IntraConnect from '@/components/IntraConnect.vue'
import UserProfile from '@/views/UserProfile.vue'
import GameLobby from '@/components/game/GameLobby.vue'
import ButtonConnect from "@/components/ButtonConnect.vue";
import Chat from "@/views/Chat.vue";
import logoutAccount from "@/components/LogoutAccount.vue";


const routes: Array<RouteRecordRaw> = [
	{ path: '/profile', component: Home, meta: {requiresAuth: true} },
	{ path: '/login', component: ButtonConnect },
	{ path: '/logout', component: logoutAccount},
	{ path: '/lobby', component: GameLobby, meta: {requiresAuth: true} },
	{ path: '/lobby/:lobbyID', component: GameLobby, props: true, meta: {requiresAuth: true} },
	{ path: '/user/:userLogin', component: UserProfile, props: true, meta: {requiresAuth: true} },
	{ path: '/game/:gameID', component: PongGame, props: true, meta: {requiresAuth: true}},
	{ path: '/game', component: PongGame, meta: {requiresAuth: true} },
	{ path: '/', name: 'HomePage', component: HomePage },
	{ path: '/chat', component: Chat, meta: {requiresAuth: true} },
	{ path: '/profile', component: Home, meta: {requiresAuth: true} },
	{ path: '/intra-connect', name: 'IntraConnect', component: IntraConnect },
	{ path: '/:pathMatch(.*)*', redirect: '/404'},
	{ path: '/:pathMatch(.*)*', redirect: '/404'},
	{ path: '/401', name: '401', component: NotAuthorized},
	{ path: '/404', name: '404', component: NotFound},
]

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes
})

router.beforeResolve(async (to, from, next) => {
	if (to.meta.requiresAuth) {
		try {
			const response = await fetch('http://' + process.env.VUE_APP_IP_HOST + ':' + process.env.VUE_APP_BACK_PORT + '/api/user', {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json'
				},
				credentials: 'include',
			});

			if (!response.ok) {
				next('/login');
				return false;
			} else if (!response) {
				next('/login');
				return false;
			} else {
				const content = await response.json();

				if (!content || content.error) {
					next('/login');
					return false;
				} else
					next();
			}
		} catch (error) {
			next('/login');
			return false;
		}
	} else
		next();
})

export default router
