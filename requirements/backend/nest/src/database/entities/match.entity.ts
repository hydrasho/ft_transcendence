import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { User } from 'src/database/entities/user.entity';

@Entity('match')
export class Match {
  @PrimaryColumn()
  id: string;

  @Column({ nullable: false })
  timestamp: Date;

  @ManyToOne(() => User, (user) => user.login, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn({ name: 'left_player_id' })
  left_player: User;

  @ManyToOne(() => User, (user) => user.login, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn({ name: 'right_player_id' })
  right_player: User;

  @Column({ nullable: false })
  left_player_score: number;

  @Column({ nullable: false })
  right_player_score: number;

  @Column({ nullable: false })
  is_left_winner: boolean;
}
