
const AVATAR_SIZE = 55

export class PlayerProfile {
	color: string;
	image: HTMLImageElement;
	username: string;
	x: number;
	y: number;

	constructor(img: string, username: string, id: 1 | 2) {
		this.image = new Image();
		this.image.src = img;
		this.username = username;
		if (id === 1) {
			this.x = 95;
			this.y = 358;
			this.color = '#ffbe42'
		} else {
			this.x = 510;
			this.y = 358;
			this.color = 'blue'
		}
	}

	draw(ctx: CanvasRenderingContext2D, ratio: number) {

		this.draw_avatar(ctx, ratio);
		this.draw_username(ctx, ratio);

		ctx.save()
		ctx.strokeStyle = this.color;
		ctx.lineWidth = 4;

		ctx.beginPath();
		ctx.arc((this.x + AVATAR_SIZE / 2) * ratio, (this.y + AVATAR_SIZE / 2) * ratio, AVATAR_SIZE * ratio / 2, 0, Math.PI * 2);
		ctx.stroke();
		ctx.restore()
	}

	private draw_avatar(ctx: CanvasRenderingContext2D, ratio: number) {
		const min = Math.min(this.image.width, this.image.height);

		const squareX = (this.image.width - min) / 2;
		const squareY = (this.image.height - min) / 2;


		ctx.save()

		ctx.beginPath()
		ctx.arc((this.x + AVATAR_SIZE / 2) * ratio, (this.y + AVATAR_SIZE / 2) * ratio, AVATAR_SIZE * ratio / 2, 0, Math.PI * 2);
		ctx.closePath()

		ctx.clip()

		ctx.drawImage(this.image, squareX, squareY, min, min, this.x * ratio, this.y * ratio, AVATAR_SIZE * ratio, AVATAR_SIZE * ratio)

		ctx.restore()

	}

	private draw_username(ctx: CanvasRenderingContext2D, ratio: number) {

		ctx.save()
		// Customize text properties
		const textColor = this.color;
		let fontSize = 24 * ratio

		if (this.username.length > 10)
			fontSize -= this.username.length * ratio / 2

		// Set font properties
		ctx.font = `${fontSize}px 'Press Start 2P'`;
		ctx.fillStyle = textColor;

		if (this.x < 300)
			ctx.textAlign = 'left';
		else
			ctx.textAlign = 'right';

		// Render retro gaming text
		if (this.x < 300)
			ctx.fillText(this.username, (this.x + AVATAR_SIZE + 10) * ratio, (this.y + 27) * ratio);
		else
			ctx.fillText(this.username, (this.x - 10) * ratio, (this.y + 52) * ratio);
		ctx.restore();
	}
}
