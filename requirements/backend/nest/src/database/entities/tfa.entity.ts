import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity('Tfa')
export class Tfa {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => User, (user1) => user1.id, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn()
  user: User;

  @Column({ unique: true, nullable: false })
  tfa_key: string;
}
