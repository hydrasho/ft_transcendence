import { Module } from '@nestjs/common';
import { UsersController } from '../users/users.controller';
import { UsersService } from '../users/users.service';
import { UsersModule } from '../users/users.module';
import { TfaService } from './tfa.service';
import { TfaController } from './tfa.controller';
import { AppModule } from '../app.module';

@Module({
  imports: [AppModule, UsersModule], // New modules must be imported here
  controllers: [TfaController, UsersController],
  providers: [TfaService, UsersService],
})
export class TfaModule {}
