import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ApiService } from '../../api/api.service';
import { channelMemberService } from '../../channelMember/channelMember.service';
import { MemberType } from '../../database/enums/MemberType.enum';
import { channelMember } from '../../database/entities/channelMember.entity';

@Injectable()
export class channelsRightGuard implements CanActivate {
  constructor(
    private apiService: ApiService,
    public channelMemberService: channelMemberService,
  ) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const body = request.body;
    const cookie: string = request.cookies['jwt'];
    if (!cookie) return false;

    const login = await this.apiService.getCookieLogin(request);
    const channelName = body.channelName;
    const targetLogin = body.login;
    if (!login || !channelName || !targetLogin) return false;

    const memberData = await this.channelMemberService.getUserInfo(
      login,
      channelName,
    );

    if (!(memberData instanceof channelMember)) return false;

    if (!memberData) return false;
    if (memberData.blocked) return false;
    if (!memberData.register) return false;
    if (memberData.type === MemberType.NORMAL && targetLogin !== login)
      return false;

    return true;
  }
}
