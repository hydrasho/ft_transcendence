import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { isJWT } from 'class-validator';

@Injectable()
export class CookieAuthGuard implements CanActivate {
  constructor(private jwtService: JwtService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    if (!request || !request.cookies) return false;
    const cookie = request.cookies['jwt'];
    const cookie2 = request.cookies['_login'];
    const intraCookie = request.cookies['_uid'];

    if (!cookie || !cookie2 || !intraCookie) return false;
    if (cookie.length < 1 || cookie2.length < 1 || intraCookie.length < 1)
      return false;

    return await this.validateCookie(cookie, cookie2);
  }

  async validateCookie(cookieValue: string, login: string): Promise<boolean> {
    try {
      if (!isJWT(cookieValue)) return false;
      const data = await this.jwtService.verifyAsync(cookieValue).catch(() => {
        return null;
      });

      if (data.err) return false;
      if (!data || data.length < 1) return false;
      if (data['login'] === login) return true;
      else {
        return false;
      }
      return true;
    } catch (error) {
      return false;
    }
  }
}
