import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity('privMsg')
export class privMsg {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.login, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn()
  sender: User;

  @ManyToOne(() => User, (user) => user.login, {
    onDelete: 'CASCADE',
    nullable: false,
  })
  @JoinColumn()
  receiver: User;

  @Column({ nullable: false })
  message: string;
}
