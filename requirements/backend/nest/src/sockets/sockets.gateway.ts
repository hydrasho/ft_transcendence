import {
  WebSocketGateway,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { ConnexionStatus, SocketsService } from './sockets.service';
import { UseGuards } from '@nestjs/common';
import { CookieAuthGuard } from 'src/guardians/cookies/cookies.guard';
import { ApiService } from 'src/api/api.service';

@WebSocketGateway({ cors: { origin: 'http://' + process.env.IP_HOST + ':' + process.env.FRONT_PORT, credentials: true } })
export class SocketsGateway implements OnGatewayConnection, OnGatewayDisconnect {
    constructor(private readonly socketsService: SocketsService,
        private readonly apiService: ApiService) { }

  @WebSocketServer() _server: Server;

  @UseGuards(CookieAuthGuard)
  async handleConnection(client: Socket) {
    const login = await this.apiService.getCookieLoginSocket(client);

    if (login) {
      this.socketsService.newConnexion(
        client.id,
        login,
        ConnexionStatus.CONNECTED,
      );
    } else {
      client._error({ message: 'Bad credentials.' });
    }
  }

  async handleDisconnect(client: Socket) {
    const login = await this.apiService.getCookieLoginSocket(client);

    this.socketsService.notifyStatusConnection(
      login,
      ConnexionStatus.DISCONNECTED,
    );
    this.socketsService.newDisconnexion(client.id);
  }

  @SubscribeMessage('GET_STATUS')
  getStatusByLogin(client: Socket, payload: {login: string}){
    return this.socketsService.getStatusByLogin(payload.login);
  }
}
