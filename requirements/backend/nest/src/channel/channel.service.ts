import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { channel } from '../database/entities/channel.entity';
import { ChannelType } from '../database/enums/ChannelType.enum';
import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';
import { SocketsService } from '../sockets/sockets.service';

@Injectable()
@WebSocketGateway({ cors: { origin: 'http://' + process.env.VUE_APP_IP_HOST + ':' + process.env.VUE_APP_FRONT_PORT, credentials: true } })
export class channelService {
  @WebSocketServer() server: Server;

  constructor(
    private readonly socketService: SocketsService,
    @InjectRepository(channel) readonly channelRepository: Repository<channel>,
  ) {}

  // METHODS ************************************ //

  /**
   * add a channel info to the database
   */
  async create(channel: channel) {
    const save = await this.channelRepository.save(channel);
    return save;
  }

  /**
   * delete a channel from the database with its id
   */
  async delete(channelId: number) {
    return await this.channelRepository.delete(channelId).catch(() => {
      return { error: 'database error' };
    });
  }

  /**
   * return all rows (dangerous)
   */
  async getAll() {
    return await this.channelRepository.find().catch(() => {
      return { error: 'database error' };
    });
  }

  /**
   * search a channel with its name
   */
  async getChannelByName(channelName: string) {
    return await this.channelRepository
      .findOne({
        where: { name: channelName },
      })
      .catch(() => {
        return { error: 'database error' };
      });
  }

  /**
   * search channels with the type
   */
  async getChannelsByType(channelType: ChannelType) {
    const channel = await this.channelRepository
      .find({
        where: { type: channelType },
      })
      .catch(() => {
        return { error: 'channel not found' };
      });
    if (channel == null) return { error: 'channel not found' };
    return channel;
  }

  /**
   * edit the password of a channel
   */
  async changePass(channel: channel, pass: string) {
    if (pass === '') pass = null;

    channel.pass = pass;

    return await this.channelRepository.save(channel).catch((error) => {
      throw error;
    });
  }

  // SOCKETS ************************************ //

  /**
   * emit a specific action on the socket
   * @param channelName
   * @param action
   * @param login
   */
  newActionEvent(channelName: string, action: string, login: string) {
    this.server.emit(' REFRESH ', {
      channelName: channelName,
      action: action,
      to: login,
    });
  }
}
