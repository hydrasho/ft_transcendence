#!/bin/ash
cd /suprapong

cat << END > .env
VUE_APP_CLIENT_ID=${CLIENT_ID}
VUE_APP_IP_HOST=${IP_HOST}
VUE_APP_BACK_PORT=${BACK_PORT}
VUE_APP_FRONT_PORT=${FRONT_PORT}
END
cp .env src/.env

yarn
npx vue-cli-service build --dest /app

exec "$@"
