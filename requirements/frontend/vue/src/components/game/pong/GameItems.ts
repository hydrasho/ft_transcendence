import * as Assets from './Assets'
import { Flash } from './Flash';
import { Vector2i } from './game.type';
import { Personnage } from './Personnage';
import { Score } from './Score';
import { Ball } from './Ball';
import { Bonus } from './Bonus';

const background = new Image();
background.src = Assets.imgBackground;

// const background_improved = new Image();
background.src = Assets.imgBackgroundImproved;
export const shift: Vector2i = {x: 108, y: 75};

const youwin = new Image();
youwin.src = Assets.imgYouWin;
const youlose = new Image();
youlose.src = Assets.imgYouLose;

export { background };
export { youwin };
export { youlose };

export const perso = new Personnage(20, 40, 'orange');
export const ennemy = new Personnage(420, 180, 'blue');
export const score1 = new Score(Assets.imgScore, 'orange');
export const score2 = new Score(Assets.imgScore, 'blue');
export const ball = new Ball(Assets.imgBall);
export const bonus = new Bonus(Assets.imgBonus);

export const flash1 = new Flash(false, Assets.imgFlash, Assets.imgPlus, 6 + shift.x, 6 + shift.y);
export const flash2 = new Flash(false, Assets.imgFlashR, Assets.imgPlus, 226 + shift.x, 6 + shift.y);
export const flash_win1 = new Flash(true, Assets.imgWinR, Assets.imgPlus, 0 + shift.x, 0 + shift.y);
export const flash_win2 = new Flash(true, Assets.imgWinL, Assets.imgPlus, 226 + shift.x, 0 + shift.y);
