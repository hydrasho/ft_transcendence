import { Module } from '@nestjs/common';
import { GameLobbyGateway } from './game_lobby.gateway';
import { GameLobbyService } from './game_lobby.service';

@Module({
  imports: [],
  controllers: [],
  providers: [GameLobbyGateway, GameLobbyService],
})
export class GameLobbyModule {}
