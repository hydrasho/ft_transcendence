import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  login: string;

  @Column({ unique: true })
  username: string;

  @Column({ nullable: false, default: false })
  using_2fa: boolean;

  @Column({ nullable: false })
  avatar_file: string;
}
